@extends('layouts.default')
@section('title', 'Invoice')
@section('content')
<div id="card_invoice" class="card">
    <div class="card-header  card-primary text-white">
        <i class="icon ion-card qwe" data-pack="default"
           data-tags="credit, price, debit, money, shopping, cash, dollars, $"></i>
        Transaksi SP2D Berhasil
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <tr>
                        <td><img src="img/banknttwithmotto.jpg" class="img-fluid" width="100"></td>
                        <td class="text-right">
                            <h3 class="text-primary">ID Transaksi: 201709{{$data[0]->id}}</h3>
                            <p class="text-gray">Tanggal: {{$data[0]->updated_at}}</p>
                        </td>
                    </tr>
                </table>
                <div class="row" style="margin-top: 20px; border-top: 1px solid #ccc;">
                    <div class="col-lg-12 mrg_btm15" style="margin-top: 20px;">
                        Transaksi berhasil diproses dengan rincian sebagai berikut:
                        <br>
                        <table>
                            <tr>
                                <td nowrap class="text-left align-text-top">No SP2D</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>{{$data[0]->No_SP2D}}</strong></td>
                            </tr>
                            <tr>
                                <td nowrap class="text-left align-text-top"> Nama Penerima</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>{{$data[0]->Nm_Penerima}}</strong></td>
                            </tr>
                            <tr>
                                <td nowrap class="text-left align-text-top">Tanggal SP2D</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>{{$data[0]->Tgl_SP2D}}</strong></td>
                            </tr>
                            <tr>
                                <td class="text-left align-text-top">Keterangan</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>{{$data[0]->Keterangan}}</strong></td>
                            </tr>
                        </table>
                        <br>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>Detail Transaksi</strong>
                    </div>
                    <div class="card-block m-t-35">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <td>
                                        <strong>Rek. Penerima</strong>
                                    </td>
                                    <td>
                                        <strong>Bank Penerima</strong>
                                    </td>
                                    <td></td>
                                    <td class="text-right">
                                        <strong>Nilai</strong>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$data[0]->Rek_Penerima}}</td>
                                    <td>{{$data[0]->Bank_Penerima}}</td>
                                    <td></td>
                                    <td class="text-right">Rp {{$data[0]->Nilai}}</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow">
                                        <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                    </td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-right">
                                        Potongan &nbsp;
                                    </td>
                                    <td class="highrow text-right">
                                        Rp 0
                                    </td>
                                </tr>
                                <tr>
                                    <td class="emptyrow">
                                        <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                    </td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-right">
                                        <strong>Total &nbsp;</strong>
                                    </td>
                                    <td class="highrow text-right">
                                        <strong>Rp {{$data[0]->Nilai}}</strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 invoice_print">
                <span class="pull-right">
                    <a id="btn_invoice_selesai" class="btn button-alignment btn-warning text-white m-t-15">
                        Selesai
                    </a>
                    <a style="color:#fff;" class="btn button-alignment btn-info m-t-15" data-toggle="button"
                       onclick="javascript:window.print();">
                        Cetak
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        PNotify.prototype.options.delay = 1500;

        $("#btn_invoice_selesai").click(function () {
            $(location).attr('href','sp2d-list');
        });
    });
</script>

@stop
