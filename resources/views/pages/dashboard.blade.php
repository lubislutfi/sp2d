@extends('layouts.default')
@section('title', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-mint">
                Sisa Saldo
            </div>
            <div class="card-block">
                <div id="gridSisaSaldo"></div>
            </div>
            <div class="card-footer bg-white">
                <a id="btn_dashboard_sisasaldo_more" class="btn btn-primary text-white pull-right">Selengkapnya</a>
            </div>
        </div>
    </div>
</div>
<div class="row m-t-25">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-success">
                Daftar SPM Terakhir
            </div>
            <div class="card-block">
                <div id="gridDashboardListSPM"></div>
            </div>
            <div class="card-footer bg-white">
                <div class="row">
                    <div class="col-md-8">Last Update: <strong>10:12:00 21-09-2017</strong></div>
                    <div class="col-md-4">
                        <a id="btn_dashboard_trxspm_more" class="btn btn-primary text-white pull-right">Selengkapnya</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-success">
                Daftar SP2D Terakhir
            </div>
            <div class="card-block">
                <div id="gridDashboardListSP2D"></div>
            </div>
            <div class="card-footer bg-white">
                <a id="btn_dashboard_trxsp2d_more" class="btn btn-primary text-white pull-right">Selengkapnya</a>
            </div>
        </div>
    </div>
</div>

<!--<div id="card_input_id" class="row m-t-25">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header card-primary text-white">
                Input Nomor ID
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-25">
                            <div class="col-lg-10 push-lg-1">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-id-card"></i></span>
                                    <input type="text" class="form-control" id="id_sp2d" placeholder="Nomor ID">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-11 text-right">
                                <a id="btn_input_id_clear" class="btn btn-warning text-white">Clear</a>
                                <a id="btn_input_id_cek" class="btn btn-primary text-white">CEK</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div id="pb_input_id" class="progress" style="display: none">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 100%"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Mengambil data
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        PNotify.prototype.options.delay = 1500;
        var usertype = "{{ Auth::user()->userType }}";
        console.log("usertype");
        console.log(usertype);

        if (usertype == 3) //redirect
        {
            $(location).attr('href', 'sp2d-list');
        }

        var sisaSaldoDummyData = [{
            "nama": "DINAS PERTANIAN DAN PETERNAKAN	",
            "norek": "00101130003022",
            "bank": "BANK NTT",
            "saldo": 1500000000,
        }, {
            "nama": "BADAN PENGELOLAH PAJAK DAN RETRIBUSI DAERAH	",
            "norek": "00101130003036",
            "bank": "BANK NTT",
            "saldo": 2123000000,
        }, {
            "nama": "DINAS KEHUTANAN DAERAH	",
            "norek": "001.01.13.000302-4	",
            "bank": "BANK NTT",
            "saldo": 3887000000,
        }, {
            "nama": "BADAN PENGELOLA KEUANGAN DAN ASET DAERAH	",
            "norek": "001.01.13.000303-7	",
            "bank": "BANK NTT",
            "saldo": 57853000000,
        }, {
            "nama": "GIRO PEMDA KAB. KUPANG",
            "norek": "00101520074601",
            "bank": "BANK NTT",
            "saldo": 45000000000,
        },];


        //sisa saldo
        var dataGrid = $("#gridSisaSaldo").dxDataGrid({
            columnAutoWidth: true,
            dataSource: sisaSaldoDummyData,
            showColumnLines: false,
            showRowLines: false,
            showBorders: false,
            columns: [
                {
                    dataField: "nama",
                    caption: "Nama",
                }, {
                    dataField: "norek",
                    caption: "No Rekening",

                }, {
                    dataField: "bank",
                    caption: "Nama Bank",

                }, {
                    dataField: "saldo",
                    caption: "Saldo",
                    alignment: "right",
                    calculateCellValue: function (e) {
                        var angka = e.saldo;
                        //console.log(e);
                        var rupiah = '';
                        var angkarev = angka.toString().split('').reverse().join('');
                        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                        return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');

                    }
                }
            ]
        }).dxDataGrid("instance");

        //sp2d grid
        var storeDashboardListSP2D = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};

                console.log(loadOptions);

                //Paging options
                if (loadOptions.skip) {
                    params.skip = loadOptions.skip;
                }

                if (loadOptions.take) {
                    params.take = loadOptions.take;
                }

                //Filter
                if (loadOptions.filter) {

                    if (!Array.isArray(loadOptions.filter[0])) {
                        var paymentFilter;

                        paymentFilter = loadOptions.filter;
                        params.filter = paymentFilter;
                    } else {
                        var paymentFilter = [];
                        for (var i = 0; i < loadOptions.filter.length; i++) {
                            if (Array.isArray(loadOptions.filter[i])) {
                                if (Array.isArray(loadOptions.filter[i][0])) {
                                    var paymentFilterChild = [];
                                    for (var j = 0; j < loadOptions.filter[i].length; j++) {
                                        if (Array.isArray(loadOptions.filter[i][j])) {
                                            if (loadOptions.filter[i][j][0] == "tanggal") {
                                                var date = loadOptions.filter[i][j][2];
                                                var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                                paymentFilterChild.push([loadOptions.filter[i][j][0], loadOptions.filter[i][j][1], date_string]);
                                            }
                                        } else {
                                            paymentFilterChild.push(loadOptions.filter[i][j]);
                                        }
                                    }
                                    paymentFilter.push(paymentFilterChild);

                                } else if (loadOptions.filter[i][0] == "tanggal") {

                                    var date = loadOptions.filter[i][2];
                                    var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                    paymentFilter.push([loadOptions.filter[i][0], loadOptions.filter[i][1], date_string]);

                                } else {
                                    paymentFilter.push(loadOptions.filter[i]);
                                }

                            } else {
                                paymentFilter.push(loadOptions.filter[i]);
                            }
                            params.filter = paymentFilter;

                        }
                    }
                }

                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                console.log("loadOptions");
                console.log(loadOptions);
                console.log("params");
                console.log(params);
                //params.periode = periodeSelected;
                params.filter = loadOptions.filter;
                params.limit = 5;

                $.ajax({
                    url: "gettrxsp2d",
                    method: 'POST',
                    data: params,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.totalCount});
                        console.log("result");
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key.id);
                console.log("---");
            },
        });
        var gridDashboardListSP2D = $("#gridDashboardListSP2D").dxDataGrid({
            columnAutoWidth: true,
            rowAlternationEnabled: false,
            dataSource: {
                store: storeDashboardListSP2D
            }/*,
             scrolling: {
             mode: 'virtual'
             }*/,
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },
            filterRow: {
                visible: false,
                applyFilter: "auto"
            }, remoteOperations: {
                paging: false,
                filtering: true,
                sorting: true,
            },
            selection: {
                mode: "single",
            },
            hoverStateEnabled: true,
            filterRow: {
                visible: true
            },
            columnChooser: {
                enabled: false,
                emptyPanelText: 'Geser kolom kesini untuk menyembunyikan'
            },
            export: {
                enabled: false,
                fileName: "Absensi Harian",
            },
            scrolling: {
                mode: 'virtual'
            },
            paging: {
                enabled: true,
                pageSize: 30
            },
            columns: [
                {
                    caption: 'Nm_Penerima',
                    allowFiltering: false,
                    dataField: 'Nm_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'No_SP2D',
                    allowFiltering: false,
                    dataField: 'No_SP2D',
                    allowEditing: false,
                }, {
                    caption: 'Nilai',
                    allowFiltering: false,
                    dataField: 'Nilai',
                    allowEditing: false,
                    alignment: "right",
                    calculateCellValue: function (e) {
                        angka = e.Nilai;
                        var rupiah = '';
                        var angkarev = angka.toString().split('').reverse().join('');
                        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                        return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');

                    }
                }, {
                    caption: 'Tgl_SP2D',
                    dataType: 'date',
                    allowFiltering: false,
                    dataField: 'Tgl_SP2D',
                    allowEditing: false,
                }, {
                    caption: 'Keterangan',
                    allowFiltering: false,
                    dataField: 'Keterangan',
                    allowEditing: false,
                }, {
                    caption: 'Cair',
                    allowFiltering: false,
                    dataField: 'Cair',
                    allowEditing: false,
                    lookup: {
                        dataSource: [{id: 0, name: "Belum Cair"}, {id: 1, name: "Sudah Cair"},],
                        displayExpr: "name",
                        valueExpr: "id"
                    }
                }
            ],
            onSelectionChanged: function (selectedItems) {
                if (selectedItems.selectedRowsData[0].No_SP2D) {
                    selectedId = selectedItems.selectedRowsData[0].No_SP2D;
                }
            },
            onRowPrepared: function (info) {
                if (info.rowType === 'data') {
                    if (info.data.Cair == 0) {

                    } else if (info.data.Cair == 1) {
                        info.rowElement.css('background', '#E8F5E9');
                    }
                }
            }
            ,
            onRowClick: function (e) {
                /*lastRowCLickedId = e.rowIndex;
                 console.log(e);*/
            }
        }).dxDataGrid("instance");

        //spm grid
        var storeDashboardListSPM = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};

                console.log(loadOptions);

                //Paging options
                if (loadOptions.skip) {
                    params.skip = loadOptions.skip;
                }

                if (loadOptions.take) {
                    params.take = loadOptions.take;
                }

                //Filter
                if (loadOptions.filter) {

                    if (!Array.isArray(loadOptions.filter[0])) {
                        var paymentFilter;

                        paymentFilter = loadOptions.filter;
                        params.filter = paymentFilter;
                    } else {
                        var paymentFilter = [];
                        for (var i = 0; i < loadOptions.filter.length; i++) {
                            if (Array.isArray(loadOptions.filter[i])) {
                                if (Array.isArray(loadOptions.filter[i][0])) {
                                    var paymentFilterChild = [];
                                    for (var j = 0; j < loadOptions.filter[i].length; j++) {
                                        if (Array.isArray(loadOptions.filter[i][j])) {
                                            if (loadOptions.filter[i][j][0] == "tanggal") {
                                                var date = loadOptions.filter[i][j][2];
                                                var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                                paymentFilterChild.push([loadOptions.filter[i][j][0], loadOptions.filter[i][j][1], date_string]);
                                            }
                                        } else {
                                            paymentFilterChild.push(loadOptions.filter[i][j]);
                                        }
                                    }
                                    paymentFilter.push(paymentFilterChild);

                                } else if (loadOptions.filter[i][0] == "tanggal") {

                                    var date = loadOptions.filter[i][2];
                                    var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                    paymentFilter.push([loadOptions.filter[i][0], loadOptions.filter[i][1], date_string]);

                                } else {
                                    paymentFilter.push(loadOptions.filter[i]);
                                }

                            } else {
                                paymentFilter.push(loadOptions.filter[i]);
                            }
                            params.filter = paymentFilter;

                        }
                    }
                }

                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                console.log("loadOptions");
                console.log(loadOptions);
                console.log("params");
                console.log(params);
                //params.periode = periodeSelected;
                params.filter = loadOptions.filter;
                params.limit = 5;

                $.ajax({
                    url: "gettrxspm",
                    method: 'POST',
                    data: params,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.totalCount});
                        console.log("result");
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key.id);
                console.log("---");
            },
        });
        var gridDashboardListSPM = $("#gridDashboardListSPM").dxDataGrid({
            columnAutoWidth: true,
            dataSource: {
                store: storeDashboardListSPM
            }/*,
             scrolling: {
             mode: 'virtual'
             }*/,
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },
            filterRow: {
                visible: false,
                applyFilter: "auto"
            }, remoteOperations: {
                paging: false,
                filtering: true,
                sorting: true,
            },
            selection: {
                mode: "single",
            },
            hoverStateEnabled: true,
            filterRow: {
                visible: true
            },
            columnChooser: {
                enabled: false,
                emptyPanelText: 'Geser kolom kesini untuk menyembunyikan'
            },
            export: {
                enabled: false,
                fileName: "Absensi Harian",
            },
            scrolling: {
                mode: 'virtual'
            },
            paging: {
                enabled: true,
                pageSize: 30
            },
            columns: [
                {
                    caption: 'Nm_Penerima',
                    allowFiltering: false,
                    dataField: 'Nm_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'No_SPM',
                    allowFiltering: false,
                    dataField: 'No_SPM',
                    allowEditing: false,
                }, {
                    caption: 'Status',
                    allowFiltering: false,
                    dataField: 'Status',
                    allowEditing: false,
                }, {
                    caption: 'DateCreate',
                    dataType: 'date',
                    allowFiltering: false,
                    dataField: 'DateCreate',
                    allowEditing: false,
                }, {
                    caption: 'Uraian',
                    allowFiltering: false,
                    dataField: 'Uraian',
                    allowEditing: false,
                },
            ],
            onSelectionChanged: function (selectedItems) {

            },
            onRowClick: function (e) {

            }, onRowPrepared: function (info) {
                if (info.rowType === 'data') {
                    if (info.data.Status == 0) {

                    } else if (info.data.Status == 1) {
                        info.rowElement.css('background', '#E8F5E9');
                    }
                    else if (info.data.Status == 2) {
                        info.rowElement.css('background', '#FFEBEE');
                    }
                    else if (info.data.Status == 3) {
                        info.rowElement.css('background', '#FFFDE7');
                    }

                }
            }

        }).dxDataGrid("instance");

        $("#btn_dashboard_sisasaldo_more").click(function () {
            //$(location).attr('href','sp2d-list');
        });
        $("#btn_dashboard_trxsp2d_more").click(function () {
            $(location).attr('href', 'sp2d-list');
        });
        $("#btn_dashboard_trxspm_more").click(function () {
            $(location).attr('href', 'spm-list');
        });
    })
    ;
</script>
@stop
