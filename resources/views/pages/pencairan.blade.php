@extends('layouts.default')
@section('title', 'Pencairan')
@section('content')
<div id="card_input_id" class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="card">
            <div class="card-header card-primary text-white">
                Input Nomor ID
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-25">
                            <div class="col-lg-10 push-lg-1">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-id-card"></i></span>
                                    <input type="text" class="form-control" id="id_sp2d" placeholder="Nomor ID">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-11 text-right">
                                <a id="btn_input_id_clear" class="btn btn-warning text-white">Clear</a>
                                <a id="btn_input_id_cek" class="btn btn-primary text-white">CEK</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div id="pb_input_id" class="progress" style="display: none">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 100%"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Mengambil data
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="card_inquiry_data" class="row" style="display: none">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header card-primary text-white">
                Data
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-35">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="No_SP2D" class=" col-form-label">No_SP2D</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="No_SP2D" class="form-control" placeholder="No_SP2D" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2  text-lg-right">
                                <label for="Nm_Penerima"
                                       class="col-form-label two_column_label_margintop">Nm_Penerima</label>
                            </div>
                            <div class="col-lg-8 col-xl-4  two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Nm_Penerima" class="form-control" placeholder="Nm_Penerima"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Kd_Urusan" class="col-form-label">Kd_Urusan</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Kd_Urusan" class="form-control" placeholder="Kd_Urusan"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Kd_Bidang" class="control-label">Kd_Bidang</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Kd_Bidang" class="form-control" placeholder="Kd_Bidang"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Kd_Unit" class="col-form-label">Kd_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Kd_Unit" class="form-control" placeholder="Kd_Unit" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Kd_Sub" class="control-label">Kd_Sub</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Kd_Sub" class="form-control" placeholder="Kd_Sub" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Tahun" class="col-form-label">Tahun</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Tahun" class="form-control" placeholder="Tahun" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_SP2D" class="control-label">Tgl_SP2D</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Tgl_SP2D" class="form-control" placeholder="Tgl_SP2D"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="No_SPM" class="col-form-label">No_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="No_SPM" class="form-control" placeholder="No_SPM" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_SPM" class="control-label">Tgl_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Tgl_SPM" class="form-control" placeholder="Tgl_SPM" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Jn_SPM" class="col-form-label">Jn_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Jn_SPM" class="form-control" placeholder="Jn_SPM" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Keterangan" class="control-label">Keterangan</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <textarea class="form-control" id="Keterangan" rows="6" placeholder="Keterangan"
                                              readonly>

                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="NPWP" class="col-form-label">NPWP</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="NPWP" class="form-control" placeholder="NPWP" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Bank_Penerima" class="control-label">Bank_Penerima</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Bank_Penerima" class="form-control"
                                           placeholder="Bank_Penerima" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Rek_Penerima" class="col-form-label">Rek_Penerima</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Rek_Penerima" class="form-control" placeholder="Rek_Penerima"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_Penguji" class="control-label">Tgl_Penguji</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Tgl_Penguji" class="form-control" placeholder="Tgl_Penguji"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nm_Bank" class="col-form-label">Nm_Bank</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Nm_Bank" class="form-control" placeholder="Nm_Bank" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="No_Rekening" class="control-label">No_Rekening</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="No_Rekening" class="form-control" placeholder="No_Rekening"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nilai" class="col-form-label">Nilai</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Nilai" class="form-control" placeholder="Nilai" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="DateCreate" class="control-label">DateCreate</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="DateCreate" class="form-control" placeholder="DateCreate"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Cair" class="col-form-label">Cair</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Cair" class="form-control" placeholder="Cair" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="TglCair" class="control-label">TglCair</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="TglCair" class="form-control" placeholder="TglCair" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Gaji" class="col-form-label">Gaji</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Gaji" class="form-control" placeholder="Gaji" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Nm_Unit" class="control-label">Nm_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Nm_Unit" class="form-control" placeholder="Nm_Unit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nm_Sub_Unit" class="col-form-label">Nm_Sub_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text" id="Nm_Sub_Unit" class="form-control" placeholder="Nm_Sub_Unit"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Uraian" class="control-label">Uraian</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text" id="Uraian" class="form-control" placeholder="Uraian" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 text-right">
                                <a id="btn_result_kembali" class="btn btn-warning text-white">Kembali</a>
                                <a id="btn_result_cairkan" class="btn btn-primary text-white">CAIRKAN</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="card_invoice" class="card" style="display: none">
    <div class="card-header  card-primary text-white">
        <i class="icon ion-card qwe" data-pack="default"
           data-tags="credit, price, debit, money, shopping, cash, dollars, $"></i>
        Transaksi SP2D Berhasil
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <tr>
                        <td><img src="img/banknttwithmotto.jpg" class="img-fluid" width="100"></td>
                        <td class="text-right">
                            <h3 class="text-primary">ID Transaksi: dfdfdf</h3>
                            <p class="text-gray">Tanggal: dfdfdf</p>
                        </td>
                    </tr>
                </table>
                <div class="row" style="margin-top: 20px; border-top: 1px solid #ccc;">

                    <div class="col-lg-12 mrg_btm15" style="margin-top: 20px;">
                        Transaksi berhasil berhasil diproses dengan rincian sebagai berikut:
                        <br>

                        <table>
                            <tr>
                                <td nowrap class="text-left align-text-top">No SP2D</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>1234123421312341234</strong></td>
                            </tr>
                            <tr>
                                <td nowrap class="text-left align-text-top"> Nama</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>John Snow</strong></td>
                            </tr>
                            <tr>
                                <td nowrap class="text-left align-text-top">Tanggal SP2D</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>25 Agustus 2017</strong></td>
                            </tr>
                            <tr>
                                <td class="text-left align-text-top">Keterangan</td>
                                <td class="text-left align-text-top">&nbsp:</td>
                                <td><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel purus ac
                                        nunc varius varius. Maecenas molestie quam a tortor placerat, sed condimentum
                                        nunc pretium. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                                        Morbi faucibus mattis justo, ac scelerisque tellus dictum sit amet. Etiam in
                                        dolor enim. </strong></td>
                            </tr>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
            <!--<div class="col-sm-12 invoice_body_billing_details">
                <div class="row justify-content-sm-between">
                    <div class="col-sm-5">
                        <div class="invoice_details">
                            <h4 class="success_txt">Detail Pencairan:</h4>
                            <strong>Ms Natalya Pery</strong>
                            <br>
                            Main Address &amp; Streen Num
                            <br>
                            Streen Name &amp; City Details
                            <br>
                            State Details
                            <br>
                            Phone Num: XXX-XXX-XXXX
                            <br>
                            Mail Id: mailid@mail.com
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="invoice_details">
                            <span class="info_txt">Shipping Details:</span><br>
                            <strong>Ms Natalya Pery</strong>
                            <br>
                            Main Address &amp; Streen Num
                            <br>
                            Streen Name &amp; City Details
                            <br>
                            State Details
                            <br>
                            Phone Num: XXX-XXX-XXXX
                            <br>
                            Mail Id: mailid@mail.com
                        </div>
                    </div>
                </div>

            </div>-->

        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>Jumlah Transaksi</strong>
                    </div>
                    <div class="card-block m-t-35">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <td>
                                        <strong>Rek. Penerima</strong>
                                    </td>
                                    <td>
                                        <strong>Bank Penerima</strong>
                                    </td>
                                    <td></td>
                                    <td class="text-right">
                                        <strong>Nilai</strong>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>234123412341234</td>
                                    <td>Mandiri</td>
                                    <td></td>
                                    <td class="text-right">Rp 23424</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow">
                                        <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                    </td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-right">
                                        <strong>Total &nbsp;</strong>
                                    </td>
                                    <td class="highrow text-right">
                                        <strong>Rp 23424</strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 invoice_print">
                <span class="pull-right">
                    <a id="btn_invoice_selesai" class="btn button-alignment btn-warning text-white m-t-15">
                        Selesai
                    </a>
                    <a style="color:#fff;" class="btn button-alignment btn-info m-t-15" data-toggle="button"
                       onclick="javascript:window.print();">
                        Cetak
                    </a>

                </span>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        PNotify.prototype.options.delay = 1500;

        $("#btn_input_id_clear").click(function () {
            $('#id_sp2d').val('');
        });

        $("#btn_input_id_cek").click(function () {

            if ($('#id_sp2d').val() == "") {
                new PNotify({
                    title: 'Periksa Kembali',
                    text: 'Nomor ID kosong, Pastikan sudah terisi',
                    type: 'info',
                    styling: 'brighttheme',
                    animate: {
                        animate: true,
                        in_class: 'bounceIn',
                        out_class: 'bounceOut'
                    }
                });
            } else {

                //00964/SP2D/1.03.01.01/2017

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var params = new Object();
                params.user_id = "1";
                params.no_sp2d = $('#id_sp2d').val();

                $.ajax({
                    url: "gettrxsp2d",
                    method: 'POST',
                    data: params,
                    success: function (result, textStatus, xhr) {
                        if (result.totalCount == 0) {

                            //data tidak ditemukan
                            console.log("Data tidak ditemukan");
                            swal({
                                title: 'UPS, Data Tidak Ditemukan',
                                text: 'ID SP2D: "' + $('#id_sp2d').val() + '", tidak ada di Database. Silahkan periksa kembali.',
                                type: 'warning',
                                confirmButtonColor: '#4fb7fe'
                            }).done();
                        } else {

                            //data ditemukan
                            console.log(result.data[0]);

                            //populating data
                            $('#No_SP2D').val(result.data[0].No_SP2D);
                            $('#Nm_Penerima').val(result.data[0].Nm_Penerima);
                            $('#Kd_Urusan').val(result.data[0].Kd_Urusan);
                            $('#Kd_Bidang').val(result.data[0].Kd_Bidang);
                            $('#Kd_Unit').val(result.data[0].Kd_Unit);
                            $('#Kd_Sub').val(result.data[0].Kd_Sub);
                            $('#Tahun').val(result.data[0].Tahun);
                            $('#Tgl_SP2D').val(result.data[0].Tgl_SP2D);
                            $('#No_SPM').val(result.data[0].No_SPM);
                            $('#Tgl_SPM').val(result.data[0].Tgl_SPM);
                            $('#Jn_SPM').val(result.data[0].Jn_SPM);
                            $('#Keterangan').val(result.data[0].Keterangan);
                            $('#NPWP').val(result.data[0].NPWP);
                            $('#Bank_Penerima').val(result.data[0].Bank_Penerima);
                            $('#Rek_Penerima').val(result.data[0].Rek_Penerima);
                            $('#Tgl_Penguji').val(result.data[0].Tgl_Penguji);
                            $('#Nm_Bank').val(result.data[0].Nm_Bank);
                            $('#No_Rekening').val(result.data[0].No_Rekening);
                            $('#Nilai').val(result.data[0].Nilai);
                            $('#DateCreate').val(result.data[0].DateCreate);
                            $('#Cair').val(result.data[0].Cair);
                            $('#TglCair').val(result.data[0].TglCair);
                            $('#Gaji').val(result.data[0].Gaji);
                            $('#Nm_Unit').val(result.data[0].Nm_Unit);
                            $('#Nm_Sub_Unit').val(result.data[0].Nm_Sub_Unit);
                            $('#Uraian').val(result.data[0].Uraian);

                            //console.log($('#id_sp2d').val());
                            $('#card_input_id').css('display', 'none');
                            $('#card_inquiry_data').css('display', 'block');
                            $('#card_invoice').css('display', 'none');
                        }

                    },
                    error: function (xhr, textStatus) {
                        console.log(xhr.status);
                        swal({
                            title: 'Gangguan Database',
                            text: 'Server tidak dapat menyambung ke Database SP2D Pusat',
                            type: 'error',
                            confirmButtonColor: '#4fb7fe'
                        }).done();
                    }
                    ,
                    // timeout: 7000
                })
                ;


            }
        });

        $("#btn_result_cairkan").click(function () {
            $('#id_sp2d').val('');
            $('#card_input_id').css('display', 'none');
            $('#card_inquiry_data').css('display', 'none');
            $('#card_invoice').css('display', 'block');
        });

        $("#btn_result_kembali").click(function () {
            $('#id_sp2d').val('');
            $('#card_input_id').css('display', 'block');
            $('#card_inquiry_data').css('display', 'none');
            $('#card_invoice').css('display', 'none');
        });

        $("#btn_invoice_selesai").click(function () {
            $('#id_sp2d').val('');
            $('#card_input_id').css('display', 'block');
            $('#card_inquiry_data').css('display', 'none');
            $('#card_invoice').css('display', 'none');
        });
    });
</script>

@stop
