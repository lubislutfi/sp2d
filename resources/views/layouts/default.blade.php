<!doctype html>
<html class="no-js" lang="en">

<head>
    <!--HEAD IMPORT-->
    @include('includes.head')
</head>

<body class="fixedNav_position fixedMenu_left P-notify">
<!--@include('includes.preloader')-->

<div id="wrap">
    <div id="top" class="fixed">
        <!-- .navbar -->
        @include('includes.navbar')
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->
    <div class="wrapper fixedNav_top">
        <div id="left" class="fixed">
            <div class="menu_scroll left_scrolled">
                <div class="left_media">
                    <div class="media user-media">
                        <div class="user-media-toggleHover">
                            <span class="fa fa-user"></span>
                        </div>
                        <div class="user-wrapper">
                            <a class="user-link" href="#">
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3"
                                     alt="User Picture"
                                     src="img/users.png">
                                <p class="user-info menu_hide" style="color: #0f0f0f">Selamat Datang,<br> {{ Auth::user()->name }}</p>
                            </a>
                        </div>
                    </div>
                    <hr/>
                </div>
                <ul id="menu">
                    @include('includes.sidebar')
                </ul>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header id="my_header" class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-6 col-sm-4">
                            <h4 class="nav_top_align">
                                @yield('title')
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div id="my_outer" class="outer">
                <div class="inner bg-light lter bg-container">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /#wrap -->
<!--FOOTER IMPORT-->
@include('includes.footerimport')
</body>
</html>