<meta charset="UTF-8">
<title>@yield('title') | SP2D Bank NTT</title>
<!--CSRF-->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="img/favicon.ico"/>

<link rel="stylesheet" type="text/css" href="vendors/dx/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="vendors/dx/css/dx.light.css" />

<!-- global styles-->
<link type="text/css" rel="stylesheet" href="css/components.css"/>
<link type="text/css" rel="stylesheet" href="css/custom.css"/>

<link type="text/css" rel="stylesheet" href="vendors/sweetalert/css/sweetalert2.min.css"/>
<link type="text/css" rel="stylesheet" href="css/pages/sweet_alert.css"/>

<link type="text/css" rel="stylesheet" href="css/pages/invoice.css">
<link type="text/css" rel="stylesheet" href="css/pages/buttons.css"/>

<link type="text/css" rel="stylesheet" href="vendors/pnotify/css/pnotify.css"/>
<link type="text/css" rel="stylesheet" href="vendors/pnotify/css/pnotify.brighttheme.css"/>
<link type="text/css" rel="stylesheet" href="vendors/animate/css/animate.min.css"/>
<link type="text/css" rel="stylesheet" href="vendors/pnotify/css/pnotify.buttons.css"/>
<link type="text/css" rel="stylesheet" href="vendors/pnotify/css/pnotify.history.css"/>
<link type="text/css" rel="stylesheet" href="vendors/pnotify/css/pnotify.mobile.css"/>
<link type="text/css" rel="stylesheet" href="vendors/sweetalert/css/sweetalert2.min.css"/>
<link type="text/css" rel="stylesheet" href="css/pages/p_notify.css"/>

<link type="text/css" rel="stylesheet" href="css/pages/form_layouts.css" />

<script type="text/javascript" src="vendors/dx/js/jquery-3.1.0.min.js"></script>


