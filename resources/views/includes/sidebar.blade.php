<!--Dashboard-->


@if ( Auth::user()->userType == 1)
<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a href="{{ url('/') }}">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
@elseif ( Auth::user()->userType == 2)

@elseif ( Auth::user()->userType == 3)
<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a href="{{ url('/') }}">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
@else
<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a href="{{ url('/') }}">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
@endif
<!--END OF DASHBOARD-->

<!--DATA-->
@if ( Auth::user()->userType == 1)
<li class="dropdown_menu {{ Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : '' }}">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="{{ Request::is('sp2d-list') ? 'active' : '' }}">
            <a href="{{ url('/sp2d-list') }}">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
        <li class="{{ Request::is('spm-list') ? 'active' : '' }}">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>
        <li class="{{ Request::is('pencairan-list') ? 'active' : '' }}">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
@elseif ( Auth::user()->userType == 2)
<li class="dropdown_menu {{ Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : '' }}">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="{{ Request::is('sp2d-list') ? 'active' : '' }}">
            <a href="{{ url('/sp2d-list') }}">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
 <!--       <li class="{{ Request::is('spm-list') ? 'active' : '' }}">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>-->
        <li class="{{ Request::is('pencairan-list') ? 'active' : '' }}">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
@elseif ( Auth::user()->userType == 3)
<li class="dropdown_menu {{ Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : '' }}">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="{{ Request::is('sp2d-list') ? 'active' : '' }}">
            <a href="{{ url('/sp2d-list') }}">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
        <!--<li class="{{ Request::is('spm-list') ? 'active' : '' }}">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>-->
        <li class="{{ Request::is('pencairan-list') ? 'active' : '' }}">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
@else

@endif
<!--END OF DATA-->

<!--1=admin,2=teller,3:supervisor,4=monitoring-->
@if ( Auth::user()->userType == 1)
<li class="dropdown_menu {{ Request::is('log-sukses')||Request::is('log-semua') ? 'active' : '' }}">
    <a href="javascript:;">
        <i class="fa fa-server"></i>
        <span class="link-title menu_hide">&nbsp; History</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="{{ Request::is('log-sukses') ? 'active' : '' }}">
            <a href="{{ url('/log-sukses') }}">
                <i class="fa fa-angle-right"></i>
                &nbsp; Pencairan Sukses
            </a>
        </li>
        <li class="{{ Request::is('log-semua') ? 'active' : '' }}">
            <a href="log-semua">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Log</span>
            </a>
        </li>
    </ul>
</li>
@elseif ( Auth::user()->userType == 2)

@elseif ( Auth::user()->userType == 3)

@else

@endif

<!--USER-->
<!--1=admin,2=teller,3:supervisor,4=monitoring-->
@if ( Auth::user()->userType == 1)
<li class="dropdown_menu {{ Request::is('users')||Request::is('branches') ? 'active' : '' }}">
    <a href="javascript:;">
        <i class="fa fa-gears"></i>
        <span class="link-title menu_hide">&nbsp; Pengaturan</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="{{ Request::is('branches-list') ? 'active' : '' }}">
            <a href="{{ url('/branches-list') }}">
                <i class="fa fa-angle-right"></i>
                <span class="link-title menu_hide">&nbsp;Cabang</span>
            </a>
        </li>
        <li class="{{ Request::is('users') ? 'active' : '' }}">
            <a href="{{ url('/users') }}">
                <i class="fa fa-angle-right"></i>
                <span class="link-title menu_hide">&nbsp;Users</span>
            </a>
        </li>
    </ul>
</li>
@elseif ( Auth::user()->userType == 2)

@elseif ( Auth::user()->userType == 3)

@else

@endif