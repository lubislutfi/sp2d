<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->id = 1;
        $role_user->name = 'maker';
        $role_user->side = 'pemda';
        $role_user->description = 'bertugas untuk membuat suatu transaksi. Ini adalah bagian pertama yang langsung berhadapan dengan data sp2d di database antara';
        $role_user->save();

        $role_user = new Role();
        $role_user->id = 2;
        $role_user->name = 'checker';
        $role_user->side = 'pemda';
        $role_user->description = 'bertugas untuk meneliti keabsahan transaksi yang telah dibuat oleh maker';
        $role_user->save();

        $role_user = new Role();
        $role_user->id = 3;
        $role_user->name = 'approval';
        $role_user->side = 'pemda';
        $role_user->description = 'bertugas untuk mengesahkan transaksi yang telah dibuat oleh maker dan telah diteliti oleh checker';
        $role_user->save();

        $role_user = new Role();
        $role_user->id = 4;
        $role_user->name = 'teller';
        $role_user->side = 'bank';
        $role_user->description = 'jika data sudah di approval oleh Approver maka, kemudian data itu akan di lempar ke teller untuk di cairkan';
        $role_user->save();

        $role_user = new Role();
        $role_user->id = 5;
        $role_user->name = 'supervisor teller';
        $role_user->side = 'bank';
        $role_user->description = 'user ini berfungsi sebagai filter terakhir untuk pencairan transaksi sp2d';
        $role_user->save();
    }
}
