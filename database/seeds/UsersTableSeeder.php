<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_maker = Role::where('name', 'maker')->first();
        $role_checker = Role::where('name', 'checker')->first();
        $role_approval = Role::where('name', 'approval')->first();
        $role_teller = Role::where('name', 'teller')->first();
        $role_supervisor_teller = Role::where('name', 'supervisor teller')->first();

        $sp2d_user = new User();
        $sp2d_user->setConnection('mysql');
        $sp2d_user->name = 'User Maker';
        $sp2d_user->password = bcrypt('123456');
        $sp2d_user->branch = 1;
        $sp2d_user->save();
        $sp2d_user->roles()->attach($role_maker);

        $sp2d_user = new User();
        $sp2d_user->setConnection('mysql');
        $sp2d_user->name = 'User Checker';
        $sp2d_user->password = bcrypt('123456');
        $sp2d_user->branch = 1;
        $sp2d_user->save();
        $sp2d_user->roles()->attach($role_checker);

        $sp2d_user = new User();
        $sp2d_user->setConnection('mysql');
        $sp2d_user->name = 'User Approval';
        $sp2d_user->password = bcrypt('123456');
        $sp2d_user->branch = 1;
        $sp2d_user->save();
        $sp2d_user->roles()->attach($role_approval);

        $sp2d_user = new User();
        $sp2d_user->setConnection('mysql');
        $sp2d_user->name = 'User Teller';
        $sp2d_user->password = bcrypt('123456');
        $sp2d_user->branch = 1;
        $sp2d_user->save();
        $sp2d_user->roles()->attach($role_teller);

        $sp2d_user = new User();
        $sp2d_user->setConnection('mysql');
        $sp2d_user->name = 'User Supervisor Teller';
        $sp2d_user->password = bcrypt('123456');
        $sp2d_user->branch = 1;
        $sp2d_user->save();
        $sp2d_user->roles()->attach($role_supervisor_teller);

       /* DB::connection('mysql')->table('users')->insert([
            'name' => 'administrator',
            'password' => bcrypt('123456'),
            'usertype' => 1,
            'branch' => 1
        ]);

        DB::connection('mysql')->table('users')->insert([
            'name' => 'supervisi',
            'password' => bcrypt('123456'),
            'usertype' => 1,
            'branch' => 1
        ]);

        DB::connection('mysql')->table('users')->insert([
            'name' => 'teller',
            'password' => bcrypt('123456'),
            'usertype' => 1,
            'supervisor_id' => 1,
            'branch' => 1
        ]);

        DB::connection('mysql')->table('users')->insert([
            'name' => 'monitoring',
            'password' => bcrypt('123456'),
            'usertype' => 1,
            'branch' => 1
        ]);*/

    }
}
