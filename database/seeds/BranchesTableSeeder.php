<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('mysql')->table('branches')->insert(['id' => 1, 'name' => 'KANTOR PUSAT', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 2, 'name' => 'UTAMA SURABAYA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 3, 'name' => 'MAUMERE', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 4, 'name' => 'Kewapante', 'branchgroup_id' => 2, 'induk' => 3]);
        DB::connection('mysql')->table('branches')->insert(['id' => 5, 'name' => 'Paga', 'branchgroup_id' => 2, 'induk' => 3]);
        DB::connection('mysql')->table('branches')->insert(['id' => 6, 'name' => 'ATAMBUA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 7, 'name' => 'Atapupu', 'branchgroup_id' => 2, 'induk' => 6]);
        DB::connection('mysql')->table('branches')->insert(['id' => 8, 'name' => 'Weluli', 'branchgroup_id' => 2, 'induk' => 6]);
        DB::connection('mysql')->table('branches')->insert(['id' => 9, 'name' => 'ENDE', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 10, 'name' => 'Wolowaru', 'branchgroup_id' => 2, 'induk' => 9]);
        DB::connection('mysql')->table('branches')->insert(['id' => 11, 'name' => 'Maurole', 'branchgroup_id' => 2, 'induk' => 9]);
        DB::connection('mysql')->table('branches')->insert(['id' => 12, 'name' => 'WAINGAPU', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 13, 'name' => 'Melolo', 'branchgroup_id' => 2, 'induk' => 12]);
        DB::connection('mysql')->table('branches')->insert(['id' => 14, 'name' => 'Lewa', 'branchgroup_id' => 2, 'induk' => 12]);
        DB::connection('mysql')->table('branches')->insert(['id' => 15, 'name' => 'Nggongi', 'branchgroup_id' => 2, 'induk' => 12]);
        DB::connection('mysql')->table('branches')->insert(['id' => 16, 'name' => 'RUTENG', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 17, 'name' => 'Reo', 'branchgroup_id' => 2, 'induk' => 16]);
        DB::connection('mysql')->table('branches')->insert(['id' => 18, 'name' => 'Waemoro', 'branchgroup_id' => 2, 'induk' => 16]);
        DB::connection('mysql')->table('branches')->insert(['id' => 19, 'name' => 'Iteng', 'branchgroup_id' => 2, 'induk' => 16]);
        DB::connection('mysql')->table('branches')->insert(['id' => 20, 'name' => 'LEWOLEBA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 21, 'name' => 'Balauring', 'branchgroup_id' => 2, 'induk' => 21]);
        DB::connection('mysql')->table('branches')->insert(['id' => 22, 'name' => 'BAJAWA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 23, 'name' => 'Aimere', 'branchgroup_id' => 2, 'induk' => 22]);
        DB::connection('mysql')->table('branches')->insert(['id' => 24, 'name' => 'LARANTUKA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 25, 'name' => 'Waiwerang', 'branchgroup_id' => 2, 'induk' => 24]);
        DB::connection('mysql')->table('branches')->insert(['id' => 26, 'name' => 'KEFAMENANU', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 27, 'name' => 'Oelolok', 'branchgroup_id' => 2, 'induk' => 26]);
        DB::connection('mysql')->table('branches')->insert(['id' => 28, 'name' => 'Mena', 'branchgroup_id' => 2, 'induk' => 26]);
        DB::connection('mysql')->table('branches')->insert(['id' => 29, 'name' => 'WAIKABUBAK', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 30, 'name' => 'KALABAHI', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 31, 'name' => 'Kabir', 'branchgroup_id' => 2, 'induk' => 30]);
        DB::connection('mysql')->table('branches')->insert(['id' => 32, 'name' => 'SOE', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 33, 'name' => 'Niki-Niki', 'branchgroup_id' => 2, 'induk' => 32]);
        DB::connection('mysql')->table('branches')->insert(['id' => 34, 'name' => 'Oinlasi', 'branchgroup_id' => 2, 'induk' => 32]);
        DB::connection('mysql')->table('branches')->insert(['id' => 35, 'name' => 'Kapan', 'branchgroup_id' => 2, 'induk' => 32]);
        DB::connection('mysql')->table('branches')->insert(['id' => 36, 'name' => 'ROTE', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 37, 'name' => 'Pantai Baru', 'branchgroup_id' => 2, 'induk' => 36]);
        DB::connection('mysql')->table('branches')->insert(['id' => 38, 'name' => 'BETUN', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 39, 'name' => 'LABUAN BAJO', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 40, 'name' => 'Lembor', 'branchgroup_id' => 2, 'induk' => 39]);
        DB::connection('mysql')->table('branches')->insert(['id' => 41, 'name' => 'MBAY', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 42, 'name' => 'Boawae', 'branchgroup_id' => 2, 'induk' => 41]);
        DB::connection('mysql')->table('branches')->insert(['id' => 43, 'name' => 'WAETABULA', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 44, 'name' => 'Elopada', 'branchgroup_id' => 2, 'induk' => 43]);
        DB::connection('mysql')->table('branches')->insert(['id' => 45, 'name' => 'BORONG', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 46, 'name' => 'ANAKALANG ', 'branchgroup_id' => 1]);
        DB::connection('mysql')->table('branches')->insert(['id' => 47, 'name' => 'SABU', 'branchgroup_id' => 1]);

    }
}
