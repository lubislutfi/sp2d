<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePencairansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('pencairans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Kd_Urusan')->nullable();
            $table->string('Kd_Bidang')->nullable();
            $table->string('Kd_Unit')->nullable();
            $table->string('Kd_Sub')->nullable();
            $table->integer('Tahun')->nullable();
            $table->string('No_SP2D');
            $table->dateTime('Tgl_SP2D');
            $table->string('No_SPM')->nullable();
            $table->dateTime('Tgl_SPM')->nullable();
            $table->string('Jn_SPM')->nullable();
            $table->string('Nm_Penerima')->nullable();
            $table->string('Keterangan')->nullable();
            $table->string('NPWP')->nullable();
            $table->string('Bank_Penerima')->nullable();
            $table->string('Rek_Penerima')->nullable();
            $table->dateTime('Tgl_Penguji')->nullable();
            $table->string('Nm_Bank')->nullable();
            $table->string('No_Rekening')->nullable();
            $table->string('Nilai')->nullable();
            $table->dateTime('DateCreate')->nullable();
            $table->string('Cair')->nullable();
            $table->dateTime('TglCair')->nullable();
            $table->integer('Gaji')->nullable();
            $table->string('Nm_Unit')->nullable();
            $table->string('Nm_Sub_Unit')->nullable();
            $table->string('Uraian')->nullable();
            $table->integer('id_teller');
            $table->integer('id_supervisor')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('tglSupervisi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pencairans');
    }
}
