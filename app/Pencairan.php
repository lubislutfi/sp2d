<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pencairan extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pencairans';
    protected $fillable = ['Kd_Urusan', 'Kd_Bidang', 'Kd_Unit', 'Kd_Sub', 'Tahun', 'No_SP2D', 'Tgl_SP2D', 'No_SPM', 'Tgl_SPM', 'Jn_SPM', 'Nm_Penerima', 'Keterangan', 'NPWP', 'Bank_Penerima', 'Rek_Penerima', 'Tgl_Penguji', 'Nm_Bank', 'No_Rekening', 'Nilai', 'DateCreate', 'Cair', 'TglCair', 'Gaji', 'Nm_Unit', 'Nm_Sub_Unit', 'Uraian', 'id_teller', 'id_supervisor', 'status', 'tglSupervisi'];

}
