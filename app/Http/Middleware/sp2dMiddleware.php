<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class sp2dMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        /*if () {
            // return redirect('home');
         }*/

        //Log::info("USER TYPE Middleware: ".$request->user()->userType);

        return $next($request);
    }
}
