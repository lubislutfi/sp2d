<?php

namespace App\Http\Controllers;

use App\Pencairan;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PencairanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.pencairan-list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Pencairan();

        $data->Kd_Urusan = $request->Kd_Urusan;
        $data->Kd_Bidang = $request->Kd_Bidang;
        $data->Kd_Unit = $request->Kd_Unit;
        $data->Kd_Sub = $request->Kd_Sub;
        $data->Tahun = $request->Tahun;
        $data->No_SP2D = $request->No_SP2D;
        $data->Tgl_SP2D = $request->Tgl_SP2D;
        $data->No_SPM = $request->No_SPM;
        $data->Tgl_SPM = $request->Tgl_SPM;
        $data->Jn_SPM = $request->Jn_SPM;
        $data->Nm_Penerima = $request->Nm_Penerima;
        $data->Keterangan = $request->Keterangan;
        $data->NPWP = $request->NPWP;
        $data->Bank_Penerima = $request->Bank_Penerima;
        $data->Rek_Penerima = $request->Rek_Penerima;
        $data->Tgl_Penguji = $request->Tgl_Penguji;
        $data->Nm_Bank = $request->Nm_Bank;
        $data->No_Rekening = $request->No_Rekening;
        $data->Nilai = $request->Nilai;
        $data->DateCreate = $request->DateCreate;
        $data->Cair = $request->Cair;
        $data->TglCair = $request->TglCair;
        $data->Gaji = $request->Gaji;
        $data->Nm_Unit = $request->Nm_Unit;
        $data->Nm_Sub_Unit = $request->Nm_Sub_Unit;
        $data->Uraian = $request->Uraian;
        $data->id_teller = $request->id_teller;
        $data->id_supervisor = $request->id_supervisor;
        $data->status = $request->status;
        $data->tglSupervisi = $request->tglSupervisi;

        $data->save();

        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //ambil data pencairan
    public function getJsonPencairan(Request $request)
    {
        $data = DB::connection('mysql')->table('pencairans')->select(DB::raw('*'));

        //filter
        if ($request->has('filter')) {
            $operatorQuery = "";

            if (!is_array($request->input('filter')[0])) {
                //filter cuma satu
                if ($request->input('filter')[1] == "startswith") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', $request->input('filter')[2] . '%');
                } else if ($request->input('filter')[1] == "endswith") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', '%' . $request->input('filter')[2]);
                } else if ($request->input('filter')[1] == "contains") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', '%' . $request->input('filter')[2] . '%');
                } else if ($request->input('filter')[1] == "notcontains") {
                    $data = $data->where($request->input('filter')[0], 'NOT LIKE', '%' . $request->input('filter')[2] . '%');
                } else {
                    $data = $data->where($request->input('filter')[0], $request->input('filter')[1], $request->input('filter')[2]);
                }
            } else {
                //FILTER LEBIH DARI SATU
                //var paymentFilter = [];
                for ($i = 0; $i < count($request->input('filter')); $i++) {

                    if (is_array($request->input('filter')[$i])) {
                        //ISI FILTER

                        if (is_array($request->input('filter')[$i][0])) {
                            //FILTER = TANGGAL DUA WHERE

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                $data = $data->where(function ($query) use ($request, $i) {
                                    $operatorSubquery = "";

                                    for ($j = 0; $j < count($request->input('filter')[$i]); $j++) {

                                        if (is_array($request->input('filter')[$i][$j])) {
                                            $date = $request->input('filter')[$i][$j][2];

                                            //AMBIL ISI FILTER (SUBQUERY)

                                            if ($operatorSubquery == "" || $operatorSubquery == "and") {
                                                //where
                                                if ($request->input('filter')[$i][$j][0] == "tanggal") {

                                                    if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                    } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                    } else {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                    }
                                                }
                                            } else {
                                                //orwhere
                                                if ($request->input('filter')[$i][$j][0] == "tanggal") {
                                                    if ($request->input('filter')[$i][$j][0] == "tanggal") {

                                                        if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                        } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                        } else {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            //AMBIL PENGHUBUNG (SUBQUERY)

                                            $operatorSubquery = $request->input('filter')[$i][$j];
                                        }
                                    }
                                    //tambahkan ke main filter
                                    // paymentFilter . push(paymentFilterChild);

                                });
                            } else {
                                $data = $data->orWhere(function ($query) use ($request, $i) {
                                    $operatorSubquery = "";

                                    for ($j = 0; $j < count($request->input('filter')[$i]); $j++) {

                                        if (is_array($request->input('filter')[$i][$j])) {
                                            $date = $request->input('filter')[$i][$j][2];

                                            //AMBIL ISI FILTER (SUBQUERY)

                                            if ($operatorSubquery == "" || $operatorSubquery == "and") {
                                                //where
                                                if ($request->input('filter')[$i][$j][0] == "tanggal") {

                                                    if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                    } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                    } else {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                    }
                                                }
                                            } else {
                                                //orwhere
                                                if ($request->input('filter')[$i][$j][0] == "tanggal") {
                                                    if ($request->input('filter')[$i][$j][0] == "tanggal") {

                                                        if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                        } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                        } else {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            //AMBIL PENGHUBUNG (SUBQUERY)

                                            $operatorSubquery = $request->input('filter')[$i][$j];
                                        }
                                    }
                                });
                            }
                        } else if ($request->input('filter')[$i][0] == "tanggal") {
                            $date = $request->input('filter')[$i][2];
                            //filter = Tanggal

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                //where
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', $date . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $date);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $date . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'NOT LIKE', '%' . $date . '%');
                                } else {
                                    $data = $data->where($request->input('filter')[$i][0], $request->input('filter')[$i][1], $date);
                                }

                            } else {
                                //orwhere
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', $date . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $date);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $date . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'NOT LIKE', '%' . $date . '%');
                                } else {
                                    $data = $data->orWhere($request->input('filter')[$i][0], $request->input('filter')[$i][1], $date);
                                }
                            }


                        } else {
                            //FILTER = BUKAN TANGGAL

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                //where
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2]);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'NOT LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else {
                                    $data = $data->where($request->input('filter')[$i][0], $request->input('filter')[$i][1], $request->input('filter')[$i][2]);
                                }
                            } else {
                                //orwhere
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2]);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'NOT LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else {
                                    $data = $data->orWhere($request->input('filter')[$i][0], $request->input('filter')[$i][1], $request->input('filter')[$i][2]);
                                }
                            }
                        }
                    } else {
                        //AMBIL PENGHUBUNG UTAMA
                        $operatorQuery = $request->input('filter')[$i];
                    }
                }
            }
        }

//order by
        if ($request->has('limit')) {
            $data = $data->limit($request->input('limit'));
        }

        //order by
        if ($request->has('orderBy')) {
            //$data = $data->orderBy($request->input('orderBy'), $request->input('orderType'));
        } else {
            //$data = $data->orderBy(DB::raw("updated_at"), "desc");
            //$data = $data->orderBy(DB::raw("name"), "asc");
        }

        //group by
        //$data = $data->groupBy(DB::raw("tanggal, name"));

        $total = $data;

        $total = $total->get()->count();

        if ($request->has('take')) {
            $data = $data->take($request->input('take'));
        }

        if ($request->has('skip')) {
            $data = $data->skip($request->input('skip'));
        }

        $data = $data->get();
        return response()->json([
            'totalCount' => $total,
            'data' => $data
        ]);
    }

    function pencairanTindakan(Request $request)
    {
        Log::info($request);
        if($request->has('id'))
        {
            Log::info("has");
            if($request->tindakan == "setuju")
            {
                Log::info("setuju");

                DB::connection('mysql')
                    ->table('pencairans')
                    ->where('No_SP2D', $request->id)
                    ->update(['status' => 2,'id_supervisor' => $request->id_supervisor]);

            }else if($request->tindakan == "tolak"){
                Log::info("tolak");
                DB::connection('mysql')
                    ->table('pencairans')
                    ->where('No_SP2D', $request->id)
                    ->update(['status' => 3,'id_supervisor' => $request->id_supervisor]);
            }else if($request->tindakan == "cair"){
                Log::info("cair");
                DB::connection('mysql')
                    ->table('pencairans')
                    ->where('No_SP2D', $request->id)
                    ->update(['status' => 4,'id_supervisor' => $request->id_supervisor]);
                DB::connection('sqlsrv')
                    ->table('TrxSP2D')
                    ->where('No_SP2D', $request->id)
                    ->update(['cair' => 1]);
            }
        }
    }

}
