<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class UsersController extends Controller
{
    protected $redirectAfterLogout = 'login';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->input('username');
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6|max:25|unique:users,name,' . $request->input('id'),
            'password' => 'required|min:3|max:20',
            'password_confirm' => 'required|min:3|max:20|same:password',
            'branch' => 'required',
            'usertype' => 'required'
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $users = new User();

            $users->name = $request->name;
            $users->password = Hash::make($request->password);
            $users->usertype = $request->usertype;
            $users->branch = $request->branch;

            $users->save();

            return 'success';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator_params = "";

        if ($request->has('password')) {
            $validator_params = [
                'name' => 'required|min:6|max:25|unique:users,name,' . $request->input('id'),
                'password' => 'min:3|max:20',
                'password_confirm' => 'min:3|max:20|same:password',
                'usertype' => 'required',
                'branch' => 'required',
            ];
        } else {
            $validator_params = [
                'name' => 'required|min:6|max:25|unique:users,name,' . $request->input('id'),
                'usertype' => 'required',
                'branch' => 'required',
            ];
        }

        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), $validator_params);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $users = User::find($id);

            $request->input('');

            $users->name = $request->input('name');
            $request->input('password') != '' ? $users->password = Hash::make($request->input('password')) : '';
            $users->usertype = $request->input('usertype');
            $users->branch = $request->input('branch');

            $users->save();

            return 'success';
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getJsonUser(Request $request)
    {
        $users = User::select(['id', 'name', 'branch','usertype','supervisor_id'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->get();

        return response()->json([
            'totalCount' => User::count(),//$totalcount,
            'data' => $users
        ]);
    }

    public function loginIndex()
    {
        if (Auth::check()) {
            return view('pages.dashboard');
        } else {
            return view('login');
        }
    }

    public function loginAuthenticate(Request $request)
    {
        Log::info($request);
        //inisialisasi data login
        $credentials =
            [
                'name' => $request->input('name'),
                'password' => $request->input('password')
            ];

        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|exists:users,name',
            'password' => 'required',
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            // Log::info("Login Validator Fail");

            return redirect('login')
                ->withErrors($validator)
                ->withInput();

        } else {
            //kalau berhasil, masuk ke dashboard
            if (Auth::attempt($credentials)) {
                //Log::info("Login Success");
                return redirect()->intended('/');
            } else {
                //Log::info("Login Fail");

                //kalau gagal, muncul pesan error username dan password salah
                $errors = new MessageBag(['username' => ['Username and/or password invalid.']]);
                // if Auth::attempt fails (wrong credentials) create a new message bag instance.
                return redirect('login')->withErrors($errors)->withInput();
            }
        }
    }

    public function logoutAuthenticate()
    {
        Auth::logout();
        //Log::info("Logout");
        return redirect($this->redirectAfterLogout);
    }


}
