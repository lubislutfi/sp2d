<?php $__env->startSection('title', 'SPM LIST'); ?>
<?php $__env->startSection('content'); ?>
<div id="card_input_id" class="row">
    <div class="col-lg-12">
        <div id="my_card_container" class="card">
            <div class="card-header card-primary text-white">
                LIST DATA SPM
            </div>
            <div class="card-block">
                <div id="gridContainer"></div>
            </div>
            <div class="card-footer bg-white">
                <div class="pull-right">Last Update: <strong>10:12:00 21-09-2017<strong></strong></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        PNotify.prototype.options.delay = 1500;

        var selectedId;

        //sembunyikan head
        $("header").hide();

        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};

                console.log(loadOptions);

                //Paging options
                if (loadOptions.skip) {
                    params.skip = loadOptions.skip;
                }

                if (loadOptions.take) {
                    params.take = loadOptions.take;
                }

                //Filter
                if (loadOptions.filter) {

                    if (!Array.isArray(loadOptions.filter[0])) {
                        var paymentFilter;

                        paymentFilter = loadOptions.filter;
                        params.filter = paymentFilter;
                    } else {
                        var paymentFilter = [];
                        for (var i = 0; i < loadOptions.filter.length; i++) {
                            if (Array.isArray(loadOptions.filter[i])) {
                                if (Array.isArray(loadOptions.filter[i][0])) {
                                    var paymentFilterChild = [];
                                    for (var j = 0; j < loadOptions.filter[i].length; j++) {
                                        if (Array.isArray(loadOptions.filter[i][j])) {
                                            if (loadOptions.filter[i][j][0] == "tanggal") {
                                                var date = loadOptions.filter[i][j][2];
                                                var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                                paymentFilterChild.push([loadOptions.filter[i][j][0], loadOptions.filter[i][j][1], date_string]);
                                            }
                                        } else {
                                            paymentFilterChild.push(loadOptions.filter[i][j]);
                                        }
                                    }
                                    paymentFilter.push(paymentFilterChild);

                                } else if (loadOptions.filter[i][0] == "tanggal") {

                                    var date = loadOptions.filter[i][2];
                                    var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                    paymentFilter.push([loadOptions.filter[i][0], loadOptions.filter[i][1], date_string]);

                                } else {
                                    paymentFilter.push(loadOptions.filter[i]);
                                }

                            } else {
                                paymentFilter.push(loadOptions.filter[i]);
                            }
                            params.filter = paymentFilter;

                        }
                    }
                }

                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                console.log("loadOptions");
                console.log(loadOptions);
                console.log("params");
                console.log(params);
                //params.periode = periodeSelected;
                params.filter = loadOptions.filter;
                $.ajax({
                    url: "gettrxspm",
                    method: 'POST',
                    data: params,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.totalCount});
                        console.log("result");
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key.id);
                console.log("---");
            },
        });

        var grid = $("#gridContainer").dxDataGrid({
            columnAutoWidth: true,
            height: ($(window).height() - $("#top").height() - $("#my_header").height() - parseInt($('.outer').css('padding-top'), 10) - parseInt($('#my_header').css('padding-top'), 10)) - 100 + "px",
            showRowLines: true,
            dataSource: {
                store: store
            }/*,
             scrolling: {
             mode: 'virtual'
             }*/,
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },
            filterRow: {
                visible: true,
                applyFilter: "auto"
            }, remoteOperations: {
                paging: false,
                filtering: true,
                sorting: true,
            },
            selection: {
                mode: "single",
            },
            hoverStateEnabled: true,
            filterRow: {
                visible: true
            },
            columnChooser: {
                enabled: true,
                emptyPanelText: 'Geser kolom kesini untuk menyembunyikan'
            },
            export: {
                enabled: false,
                fileName: "Absensi Harian",
            },
            scrolling: {
                mode: 'virtual'
            },
            paging: {
                enabled: true,
                pageSize: 30
            },
            columns: [
                {
                    caption: 'Tahun',
                    visible:false,
                    allowFiltering: false,
                    dataField: 'Tahun',
                    allowEditing: false,
                }, {
                    caption: 'No_SPM',
                    allowFiltering: true,
                    dataField: 'No_SPM',
                    allowEditing: false,
                }, {
                    caption: 'Nm_Penerima',
                    allowFiltering: true,
                    dataField: 'Nm_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'Bank_Penerima',
                    visible:false,
                    allowFiltering: true,
                    dataField: 'Bank_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'Rek_Penerima',
                    allowFiltering: true,
                    dataField: 'Rek_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'Status',
                    allowFiltering: true,
                    dataField: 'Status',
                    allowEditing: false,
                }, {
                    caption: 'DateCreate',
                    dataType: 'date',
                    visible:false,
                    allowFiltering: false,
                    dataField: 'DateCreate',
                    allowEditing: false,
                }, {
                    caption: 'Uraian',
                    allowFiltering: true,
                    dataField: 'Uraian',
                    allowEditing: false,
                },
            ],
            onSelectionChanged: function (selectedItems) {
                if (selectedItems.selectedRowsData[0].No_SP2D) {
                    selectedId = selectedItems.selectedRowsData[0].No_SP2D;
                }
            },
            onRowClick: function (e) {
                /*lastRowCLickedId = e.rowIndex;
                 console.log(e);*/
            },
            onRowPrepared: function (info) {
                if (info.rowType === 'data') {
                    if (info.data.Status == 0) {

                    } else if (info.data.Status == 1) {
                        info.rowElement.css('background', '#E8F5E9');
                    }
                    else if (info.data.Status == 2) {
                        info.rowElement.css('background', '#FFEBEE');
                    }
                    else if (info.data.Status == 3) {
                        info.rowElement.css('background', '#FFFDE7');
                    }

                }
            }
        }).dxDataGrid("instance");

        $("#btn_proses_sp2d").click(function () {
            console.log(selectedId);
            if (selectedId) {
                $(location).attr('href', 'sp2d-detail?id=' + selectedId);
            } else {
                DevExpress.ui.notify('Silahkan pilih data dulu', 'warning', 600);
            }
        });
    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>