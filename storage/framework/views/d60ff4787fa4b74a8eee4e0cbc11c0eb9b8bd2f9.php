<?php $__env->startSection('title', 'SP2D Detail'); ?>
<?php $__env->startSection('content'); ?>
<div id="card_inquiry_data" class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header card-primary text-white">
                Detail Data
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-35">
                            <div class="col-md-3 col-xl-2 text-lg-right">
                                <label for="No_SP2D" class=" col-form-label">No_SP2D</label>
                            </div>
                            <div class="col-md-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->No_SP2D); ?>" id="No_SP2D" class="form-control" placeholder="No_SP2D" readonly>
                                </div>
                            </div>
                            <div class="col-md-3 col-xl-2  text-lg-right">
                                <label for="Nm_Penerima"
                                       class="col-form-label two_column_label_margintop">Nm_Penerima</label>
                            </div>
                            <div class="col-md-8 col-xl-4  two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Nm_Penerima); ?>" id="Nm_Penerima" class="form-control" placeholder="Nm_Penerima"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Kd_Urusan" class="col-form-label">Kd_Urusan</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Kd_Urusan); ?>" id="Kd_Urusan" class="form-control" placeholder="Kd_Urusan"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Kd_Bidang" class="control-label">Kd_Bidang</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Kd_Bidang); ?>" id="Kd_Bidang" class="form-control" placeholder="Kd_Bidang"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Kd_Unit" class="col-form-label">Kd_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Kd_Unit); ?>" id="Kd_Unit" class="form-control" placeholder="Kd_Unit" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Kd_Sub" class="control-label">Kd_Sub</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Kd_Sub); ?>" id="Kd_Sub" class="form-control" placeholder="Kd_Sub" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Tahun" class="col-form-label">Tahun</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Tahun); ?>" id="Tahun" class="form-control" placeholder="Tahun" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_SP2D" class="control-label">Tgl_SP2D</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Tgl_SP2D); ?>" id="Tgl_SP2D" class="form-control" placeholder="Tgl_SP2D"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="No_SPM" class="col-form-label">No_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->No_SPM); ?>" id="No_SPM" class="form-control" placeholder="No_SPM" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_SPM" class="control-label">Tgl_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Tgl_SPM); ?>" id="Tgl_SPM" class="form-control" placeholder="Tgl_SPM" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Jn_SPM" class="col-form-label">Jn_SPM</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Jn_SPM); ?>" id="Jn_SPM" class="form-control" placeholder="Jn_SPM" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Keterangan" class="control-label">Keterangan</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <textarea class="form-control" id="Keterangan" rows="6" placeholder="Keterangan"
                                              readonly><?php echo e($data[0]->Keterangan); ?>


                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="NPWP" class="col-form-label">NPWP</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->NPWP); ?>" id="NPWP" class="form-control" placeholder="NPWP" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Bank_Penerima" class="control-label">Bank_Penerima</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Bank_Penerima); ?>" id="Bank_Penerima" class="form-control"
                                           placeholder="Bank_Penerima" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Rek_Penerima" class="col-form-label">Rek_Penerima</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Rek_Penerima); ?>" id="Rek_Penerima" class="form-control" placeholder="Rek_Penerima"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Tgl_Penguji" class="control-label">Tgl_Penguji</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Tgl_Penguji); ?>" id="Tgl_Penguji" class="form-control" placeholder="Tgl_Penguji"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nm_Bank" class="col-form-label">Nm_Bank</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Nm_Bank); ?>" id="Nm_Bank" class="form-control" placeholder="Nm_Bank" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="No_Rekening" class="control-label">No_Rekening</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->No_Rekening); ?>" id="No_Rekening" class="form-control" placeholder="No_Rekening"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nilai" class="col-form-label">Nilai</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Nilai); ?>" id="Nilai" class="form-control" placeholder="Nilai" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="DateCreate" class="control-label">DateCreate</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->DateCreate); ?>" id="DateCreate" class="form-control" placeholder="DateCreate"
                                           readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Cair" class="col-form-label">Cair</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Cair); ?>" id="Cair" class="form-control" placeholder="Cair" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="TglCair" class="control-label">TglCair</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->TglCair); ?>" id="TglCair" class="form-control" placeholder="TglCair" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Gaji" class="col-form-label">Gaji</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Gaji); ?>" id="Gaji" class="form-control" placeholder="Gaji" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Nm_Unit" class="control-label">Nm_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Nm_Unit); ?>" id="Nm_Unit" class="form-control" placeholder="Nm_Unit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 col-xl-2 text-lg-right">
                                <label for="Nm_Sub_Unit" class="col-form-label">Nm_Sub_Unit</label>
                            </div>
                            <div class="col-lg-8 col-xl-4">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Nm_Sub_Unit); ?>" id="Nm_Sub_Unit" class="form-control" placeholder="Nm_Sub_Unit"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-2 two_column_label_margintop text-lg-right">
                                <label for="Uraian" class="control-label">Uraian</label>
                            </div>
                            <div class="col-lg-8 col-xl-4 two_column_signup_margintop">
                                <div class="input-group">
                                    <input type="text"  value="<?php echo e($data[0]->Uraian); ?>" id="Uraian" class="form-control" placeholder="Uraian" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 text-right">
                                <a id="btn_result_batal" class="btn btn-warning text-white">Batal</a>
                                <a id="btn_result_proses" class="btn btn-primary text-white">PROSES</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        PNotify.prototype.options.delay = 1500;

        $("#btn_result_proses").click(function () {
            var selectedId = "<?php echo e($data[0]->No_SP2D); ?>";
            var idTeller = "<?php echo e(Auth::user()->id); ?>";
            var idSupervisor = "<?php echo e(Auth::user()->supervisor_id); ?>";
            var status = "1";

            $.ajax({
                url: 'pencairan-list',
                type: 'POST',
                data: {
                    'Kd_Urusan': $("#Kd_Urusan").val(),
                    'Kd_Bidang': $("#Kd_Bidang").val(),
                    'Kd_Unit': $("#Kd_Unit").val(),
                    'Kd_Sub': $("#Kd_Sub").val(),
                    'Tahun': $("#Tahun").val(),
                    'No_SP2D': $("#No_SP2D").val(),
                    'Tgl_SP2D': $("#Tgl_SP2D").val(),
                    'No_SPM': $("#No_SPM").val(),
                    'Tgl_SPM': $("#Tgl_SPM").val(),
                    'Jn_SPM': $("#Jn_SPM").val(),
                    'Nm_Penerima': $("#Nm_Penerima").val(),
                    'Keterangan': $("#Keterangan").val(),
                    'NPWP': $("#NPWP").val(),
                    'Bank_Penerima': $("#Bank_Penerima").val(),
                    'Rek_Penerima': $("#Rek_Penerima").val(),
                    'Tgl_Penguji': $("#Tgl_Penguji").val(),
                    'Nm_Bank': $("#Nm_Bank").val(),
                    'No_Rekening': $("#No_Rekening").val(),
                    'Nilai': $("#Nilai").val(),
                    'DateCreate': $("#DateCreate").val(),
                    'Cair': $("#Cair").val(),
                    'TglCair': $("#TglCair").val(),
                    'Gaji': $("#Gaji").val(),
                    'Nm_Unit': $("#Nm_Unit").val(),
                    'Nm_Sub_Unit': $("#Nm_Sub_Unit").val(),
                    'Uraian': $("#Uraian").val(),
                    'id_teller': idTeller,
                    'id_supervisor': idSupervisor,
                    'status': status,
                    'tglSupervisi': ""
                },
                success: function (result) {
                    console.log(result);
                    if (result == 'success') {
                        DevExpress.ui.notify('berhasil disimpan', 'success', 600);
                        $(location).attr('href', 'pencairan-list');


                    } else {
                        DevExpress.ui.notify('Tidak dapat menyimpan', 'warning', 600);
                    }
                }
            });

            if(selectedId){
               // $(location).attr("href","sp2d-invoice?id="+selectedId);

            }else{
                //DevExpress.ui.notify('Silahkan pilih data dulu', 'warning', 600);
            }
        });

        $("#btn_result_batal").click(function () {
            $(location).attr('href','sp2d-list');
        });

    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>