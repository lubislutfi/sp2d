<?php $__env->startSection('title', 'Users'); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                Data User
            </div>
            <div class="card-block">
                <div id="gridContainer"></div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                Form User
            </div>
            <div class="card-block">
                <form id="myUserForm">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control"
                           placeholder="Masukkan Username">
                    <p></p>
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control"
                           placeholder="Masukkan Password">
                    <p></p>
                    <label for="password_confirm">Password (Lagi)</label>
                    <input type="password" name="password_confirm" id="password_confirm" class="form-control"
                           placeholder="Masukkan Password Lagi">
                    <p></p>

                    <div class="divBranch">
                        <label for="branch">Cabang</label>
                        <select name="branch" id="branch" class="form-control">

                        </select>
                    </div>

                    <p></p>
                    <p></p>
                    <label for="usertype">Tipe User</label>
                    <select name="usertype" id="usertype" class="form-control">
                        <option value="1">Admin</option>
                        <option value="2" selected>Teller</option>
                        <option value="3">Supervisi</option>
                        <option value="4">Monitoring</option>
                    </select>
                    <p></p>
                    <a id="btnSubmit" class="btn btn-primary text-white pull-right">Buat Baru</a>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        console.log($('meta[name="csrf-token"]').attr('content'));

        var selectedId;
        var url;
        var method;
        var type;
        var branchesList;
        var userTypeList = [{
            id: 1,
            name: "Administrator"
        }, {
            id: 2,
            name: "Teller"
        }, {
            id: 3,
            name: "Supervisi"
        }, {
            id: 4,
            name: "Monitoring"
        }];
        var userList;

        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};
                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                //args.skip = loadOptions.skip || 0;
                //args.take = loadOptions.take || 12;
                //args.filterOptions = loadOptions.filter ? JSON.stringify(loadOptions.filter) : "";
                //args.page = (loadOptions.skip + loadOptions.take) / loadOptions.take;
                console.log(loadOptions);
                $.ajax({
                    url: "getjsonuser",
                    data: params,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.total});
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key);
            },
        });

        $.ajax({
            url: "getjsonbranches",
            method: 'POST',
            success: function (result) {
                branchesList = result.data;
                console.log("result");
                console.log(branchesList);

                //populate select option at html
                for(var i = 0;i<branchesList.length;i++)
                {
                    $('#myUserForm #branch').append("<option value='"+branchesList[i].id+"'>"+branchesList[i].name+"</option>");
                }
                //kosongkan dulu selector nya
                $("div.divBranch select").val("");

                //ambil json user
                $.ajax({
                    url: "getjsonuser",
                    method: 'get',
                    success: function (result) {
                        console.log("result function");
                        console.log(result.data);
                        userList = result.data;

                        var grid = $("#gridContainer").dxDataGrid({
                            height: "100%",
                            dataSource: {
                                store: store
                            },
                            selection: {
                                mode: "single"
                            },
                            hoverStateEnabled: true,
                            paging: {
                                pageSize: 15
                            },
                            columns: [
                                {
                                    caption: 'Nama',
                                    dataField: 'name',
                                    sortOrder: 'desc'
                                }, {
                                    caption: 'Jenis User',
                                    dataField: 'usertype',
                                    lookup: {
                                        dataSource: userTypeList,
                                        displayExpr: "name",
                                        valueExpr: "id"
                                    }, /*
                                     calculateCellValue: function (data) {
                                     return (data.usertype == 0) ? "Petugas" : "Administrator";
                                     }*/
                                }, {
                                    caption: 'Cabang',
                                    dataField: 'branch',
                                    sortOrder: 'asc',
                                    lookup: {
                                        dataSource: branchesList,
                                        displayExpr: "name",
                                        valueExpr: "id"
                                    },
                                }, {
                                    dataField: 'supervisor_id',
                                    caption: 'Supervisor',
                                    lookup: {
                                        dataSource: userList,
                                        displayExpr: "name",
                                        valueExpr: "usertype"
                                    }
                                }
                            ],
                            onSelectionChanged: function (selectedItems) {
                                selectedId = selectedItems.selectedRowsData[0].id;
                                $("#id").val(selectedItems.selectedRowsData[0].id);
                                $("#name").val(selectedItems.selectedRowsData[0].name);
                                $("#usertype").val(selectedItems.selectedRowsData[0].usertype);
                                $("div.divBranch select").val(selectedItems.selectedRowsData[0].branch);

                                if (selectedId) {
                                    $('#btnSubmit').text("Edit");
                                    url = 'users/' + $("#id").val();
                                    method = 'PATCH';
                                    type = 'edit';
                                }
                            }
                        }).dxDataGrid("instance");


                    },
                    error: function () {
                        return [{}];
                    },
                    // timeout: 7000
                });
            },
            error: function () {

            },
            // timeout: 7000
        })


        $("#refresh_form").click(function () {
            $("#id").val("");
            $("#name").val("");
            $("#password").val("");
            $("#password_confirm").val("");
            $("#usertype").val("0");
            $('#btnSubmit').text("Buat Baru");
            url = 'users';
            method = 'POST';
            type = 'create';
        });

        $("#btnSubmit").click(function () {

            //menentukan method submit, edit atau buatbaru
            //defining edit or create
            if ($("#id").val() == '') {
                url = 'users';
                method = 'POST';
                type = 'create';
            } else {
                url = 'users/' + $("#id").val();
                method = 'PATCH';
                type = 'edit';
            }

            if ($("#password").val() == "") {
                data = {
                    'id': $("#id").val(),
                    'name': $("#name").val(),
                    'usertype': $("#usertype").val(),
                };
            } else {
                data = {
                    'id': $("#id").val(),
                    'name': $("#name").val(),
                    'password': $("#password").val(),
                    'password_confirm': $("#password_confirm").val(),
                    'usertype': $("#usertype").val(),
                };
            }

            //console.log(url);
            console.log(data);

            $.ajax({
                url: url,
                type: method,
                data: data,
                success: function (result) {

                    console.log(result);
                    //console.log(result.confirm_password);

                    //window.location.replace("/pengaturan-user");
                    if (result == 'success') {
                        $("#alert_panel").hide();
                        console.log('success');
                        DevExpress.ui.notify('Berhasil Disimpan', 'success', 600);
                        $("#id").val("");
                        $("#name").val("");
                        $("#password").val("");
                        $("#password_confirm").val("");
                        $("#usertype").val("0");
                        grid.refresh();
                        //window.location.replace("users");
                    } else {

                        $("#alert_panel").show();

                        // variable keperluan pembuatan alert
                        var node = document.createTextNode(result[key]);
                        var ul = document.createElement("ul");
                        var element = document.getElementById("alert_container");

                        //clear child
                        while (element.hasChildNodes()) {
                            element.removeChild(element.lastChild);
                        }

                        //window.location.replace("/pengaturan-user");
                        //ambil pesan error
                        for (var key in result) {

                            var li = document.createElement("li");
                            var node = document.createTextNode(result[key]);
                            ul.appendChild(li);
                            li.appendChild(node);
                            element.appendChild(ul);

                            // console.log(' name=' + key + ' value=' + result[key]);
                        }
                    }
                }
            });

        });
    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>