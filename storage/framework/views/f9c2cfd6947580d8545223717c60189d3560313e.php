
<script type="text/javascript" src="js/components.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<script type="text/javascript" src="vendors/sweetalert/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="vendors/Buttons/js/buttons.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.animate.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.buttons.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.confirm.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.nonblock.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.mobile.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.desktop.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.history.js"></script>
<script type="text/javascript" src="vendors/pnotify/js/pnotify.callbacks.js"></script>
<script type="text/javascript" src="vendors/sweetalert/js/sweetalert2.min.js"></script>



<!-- A DevExtreme library -->
<script type="text/javascript" src="vendors/dx/js/dx.viz-web.js"></script>

<script type="text/javascript" src="vendors/dx/js/cldr.min.js"></script>
<script type="text/javascript" src="vendors/dx/js/event.min.js"></script>
<script type="text/javascript" src="vendors/dx/js/supplemental.min.js"></script>
<script type="text/javascript" src="vendors/dx/js/globalize.js"></script>
<script type="text/javascript" src="vendors/dx/js/message.js"></script>
<script type="text/javascript" src="vendors/dx/js/number.js"></script>
<script type="text/javascript" src="vendors/dx/js/currency.js"></script>
<script type="text/javascript" src="vendors/dx/js/date.js"></script>

<script type="text/javascript" src="vendors/dx/js/jszip.min.js"></script>