<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">

        <!-- App title -->
        <title>Login | Sistem SP2D</title>

        <!-- App CSS -->
        <link href="css/login/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/login/core.css" rel="stylesheet" type="text/css" />
        <link href="css/login/components.css" rel="stylesheet" type="text/css" />
        <link href="css/login/icons.css" rel="stylesheet" type="text/css" />
        <link href="css/login/pages.css" rel="stylesheet" type="text/css" />
        <link href="css/login/menu.css" rel="stylesheet" type="text/css" />
        <link href="css/login/responsive.css" rel="stylesheet" type="text/css" />

        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">

            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                     <a href="#" class="logo"><img src="img/banknttwithtext_small.png" width="200"></a>
					<br>

				 <h4 class=" m-t-5 font-500">Melayani dengan Sungguh</h4>
				<h4 class="text-muted m-t-25 font-500">Login Sistem SP2D</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal m-t-20" action="index.html">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Username">
                            </div>
                            <div class="col-xs-12">
                                <?php if($errors->has('name')): ?>
                                <br><span class="help-block"> <strong><?php echo e($errors->first('name')); ?></strong></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password">
                            </div>
                            <div class="col-xs-12">
                                <?php if($errors->has('password')): ?>
                                <br><span class="help-block"> <strong><?php echo e($errors->first('password')); ?></strong></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end card-box-->            
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>	
	</body>
</html>
