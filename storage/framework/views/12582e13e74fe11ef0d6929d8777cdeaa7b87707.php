<!--Dashboard-->


<?php if( Auth::user()->userType == 1): ?>
<li class="<?php echo e(Request::is('/') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/')); ?>">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
<?php elseif( Auth::user()->userType == 2): ?>

<?php elseif( Auth::user()->userType == 3): ?>
<li class="<?php echo e(Request::is('/') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/')); ?>">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
<?php else: ?>
<li class="<?php echo e(Request::is('/') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/')); ?>">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
<?php endif; ?>
<!--END OF DASHBOARD-->

<!--DATA-->
<?php if( Auth::user()->userType == 1): ?>
<li class="dropdown_menu <?php echo e(Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('sp2d-list') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/sp2d-list')); ?>">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
        <li class="<?php echo e(Request::is('spm-list') ? 'active' : ''); ?>">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>
        <li class="<?php echo e(Request::is('pencairan-list') ? 'active' : ''); ?>">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
<?php elseif( Auth::user()->userType == 2): ?>
<li class="dropdown_menu <?php echo e(Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('sp2d-list') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/sp2d-list')); ?>">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
 <!--       <li class="<?php echo e(Request::is('spm-list') ? 'active' : ''); ?>">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>-->
        <li class="<?php echo e(Request::is('pencairan-list') ? 'active' : ''); ?>">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
<?php elseif( Auth::user()->userType == 3): ?>
<li class="dropdown_menu <?php echo e(Request::is('sp2d-list')||Request::is('spm-list')||Request::is('pencairan-list') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-database"></i>
        <span class="link-title menu_hide">&nbsp; Data</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('sp2d-list') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/sp2d-list')); ?>">
                <i class="fa fa-angle-right"></i>
                &nbsp; SP2D
            </a>
        </li>
        <!--<li class="<?php echo e(Request::is('spm-list') ? 'active' : ''); ?>">
            <a href="spm-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;SPM</span>
            </a>
        </li>-->
        <li class="<?php echo e(Request::is('pencairan-list') ? 'active' : ''); ?>">
            <a href="pencairan-list">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Pencairan</span>
            </a>
        </li>
    </ul>
</li>
<?php else: ?>

<?php endif; ?>
<!--END OF DATA-->

<!--1=admin,2=teller,3:supervisor,4=monitoring-->
<?php if( Auth::user()->userType == 1): ?>
<li class="dropdown_menu <?php echo e(Request::is('log-sukses')||Request::is('log-semua') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-server"></i>
        <span class="link-title menu_hide">&nbsp; History</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('log-sukses') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/log-sukses')); ?>">
                <i class="fa fa-angle-right"></i>
                &nbsp; Pencairan Sukses
            </a>
        </li>
        <li class="<?php echo e(Request::is('log-semua') ? 'active' : ''); ?>">
            <a href="log-semua">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Log</span>
            </a>
        </li>
    </ul>
</li>
<?php elseif( Auth::user()->userType == 2): ?>

<?php elseif( Auth::user()->userType == 3): ?>

<?php else: ?>

<?php endif; ?>

<!--USER-->
<!--1=admin,2=teller,3:supervisor,4=monitoring-->
<?php if( Auth::user()->userType == 1): ?>
<li class="dropdown_menu <?php echo e(Request::is('users')||Request::is('branches') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-gears"></i>
        <span class="link-title menu_hide">&nbsp; Pengaturan</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('branches-list') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/branches-list')); ?>">
                <i class="fa fa-angle-right"></i>
                <span class="link-title menu_hide">&nbsp;Cabang</span>
            </a>
        </li>
        <li class="<?php echo e(Request::is('users') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/users')); ?>">
                <i class="fa fa-angle-right"></i>
                <span class="link-title menu_hide">&nbsp;Users</span>
            </a>
        </li>
    </ul>
</li>
<?php elseif( Auth::user()->userType == 2): ?>

<?php elseif( Auth::user()->userType == 3): ?>

<?php else: ?>

<?php endif; ?>