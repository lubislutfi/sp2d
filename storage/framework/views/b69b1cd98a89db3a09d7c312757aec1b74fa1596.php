<!doctype html>
<html class="no-js" lang="en">

<head>
    <!--HEAD IMPORT-->
    <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>

<body class="fixedNav_position fixedMenu_left P-notify">
<!--<?php echo $__env->make('includes.preloader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>-->

<div id="wrap">
    <div id="top" class="fixed">
        <!-- .navbar -->
        <?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->
    <div class="wrapper fixedNav_top">
        <div id="left" class="fixed">
            <div class="menu_scroll left_scrolled">
                <div class="left_media">
                    <div class="media user-media">
                        <div class="user-media-toggleHover">
                            <span class="fa fa-user"></span>
                        </div>
                        <div class="user-wrapper">
                            <a class="user-link" href="#">
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3"
                                     alt="User Picture"
                                     src="img/users.png">
                                <p class="user-info menu_hide" style="color: #0f0f0f">Selamat Datang,<br> <?php echo e(Auth::user()->name); ?></p>
                            </a>
                        </div>
                    </div>
                    <hr/>
                </div>
                <ul id="menu">
                    <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </ul>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header id="my_header" class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-6 col-sm-4">
                            <h4 class="nav_top_align">
                                <?php echo $__env->yieldContent('title'); ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div id="my_outer" class="outer">
                <div class="inner bg-light lter bg-container">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /#wrap -->
<!--FOOTER IMPORT-->
<?php echo $__env->make('includes.footerimport', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>