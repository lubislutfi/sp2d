<?php $__env->startSection('title', 'SP2D LIST'); ?>
<?php $__env->startSection('content'); ?>
<div id="card_input_id" class="row">
    <div class="col-lg-12">
        <div id="my_card_container" class="card">
            <div class="card-header card-primary text-white">
                LIST DATA PENCAIRAN
            </div>
            <div class="card-block">
                <div id="gridContainer"></div>
            </div>
            <div class="card-footer bg-white">
                <!--1=admin,2=teller,3:supervisor,4=monitoring-->
                <?php if( Auth::user()->userType == 1): ?>

                <?php elseif( Auth::user()->userType == 3): ?>
                <a id="btn_proses_sp2d" class="btn btn-primary text-white pull-right">Setujui</a>
                <a id="btn_tolak_sp2d" class="btn btn-danger text-white pull-right">Tolak</a>
                <?php elseif( Auth::user()->userType == 2): ?>
                <a id="btn_cairkan_sp2d" class="btn btn-primary text-white pull-right">Cairkan</a>
                <?php else: ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        PNotify.prototype.options.delay = 1500;

        var selectedId;
        var idTeller = "<?php echo e(Auth::user()->id); ?>";
        var id_supervisor = "<?php echo e(Auth::user()->supervisor_id); ?>";
        var status;
        var statusList = [{
            id: 1,
            name: "Menunggu Konfirm"
        }, {
            id: 2,
            name: "Disetujui"
        }, {
            id: 3,
            name: "Ditolak"
        }, {
            id: 4,
            name: "Cair"
        }];

        //sembunyikan head
        $("header").hide();


        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};

                console.log(loadOptions);

                //Paging options
                if (loadOptions.skip) {
                    params.skip = loadOptions.skip;
                }

                if (loadOptions.take) {
                    params.take = loadOptions.take;
                }

                //Filter
                if (loadOptions.filter) {

                    if (!Array.isArray(loadOptions.filter[0])) {
                        var paymentFilter;

                        paymentFilter = loadOptions.filter;
                        params.filter = paymentFilter;
                    } else {
                        var paymentFilter = [];
                        for (var i = 0; i < loadOptions.filter.length; i++) {
                            if (Array.isArray(loadOptions.filter[i])) {
                                if (Array.isArray(loadOptions.filter[i][0])) {
                                    var paymentFilterChild = [];
                                    for (var j = 0; j < loadOptions.filter[i].length; j++) {
                                        if (Array.isArray(loadOptions.filter[i][j])) {
                                            if (loadOptions.filter[i][j][0] == "tanggal") {
                                                var date = loadOptions.filter[i][j][2];
                                                var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                                paymentFilterChild.push([loadOptions.filter[i][j][0], loadOptions.filter[i][j][1], date_string]);
                                            }
                                        } else {
                                            paymentFilterChild.push(loadOptions.filter[i][j]);
                                        }
                                    }
                                    paymentFilter.push(paymentFilterChild);

                                } else if (loadOptions.filter[i][0] == "tanggal") {

                                    var date = loadOptions.filter[i][2];
                                    var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                    paymentFilter.push([loadOptions.filter[i][0], loadOptions.filter[i][1], date_string]);

                                } else {
                                    paymentFilter.push(loadOptions.filter[i]);
                                }

                            } else {
                                paymentFilter.push(loadOptions.filter[i]);
                            }
                            params.filter = paymentFilter;

                        }
                    }
                }

                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                console.log("loadOptions");
                console.log(loadOptions);
                console.log("params");
                console.log(params);
                //params.periode = periodeSelected;
                params.filter = loadOptions.filter;
                $.ajax({
                    url: "getjsonpencairan",
                    method: 'POST',
                    data: params,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.totalCount});
                        console.log("result");
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key.id);
                console.log("---");
            },
        });

        var grid = $("#gridContainer").dxDataGrid({
            columnAutoWidth: true,
            height: ($(window).height() - $("#top").height() - $("#my_header").height() - parseInt($('.outer').css('padding-top'), 10) - parseInt($('#my_header').css('padding-top'), 10)) - 100 + "px",
            dataSource: {
                store: store
            }/*,
             scrolling: {
             mode: 'virtual'
             }*/,
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },
            filterRow: {
                visible: true,
                applyFilter: "auto"
            }, remoteOperations: {
                paging: false,
                filtering: true,
                sorting: true,
            },
            selection: {
                mode: "single",
            },
            hoverStateEnabled: true,
            filterRow: {
                visible: true
            },
            columnChooser: {
                enabled: true,
                emptyPanelText: 'Geser kolom kesini untuk menyembunyikan'
            },
            export: {
                enabled: false,
                fileName: "Absensi Harian",
            },
            scrolling: {
                mode: 'virtual'
            },
            paging: {
                enabled: true,
                pageSize: 30
            },
            columns: [
                {
                    caption: 'status',
                    allowFiltering: false,
                    dataField: 'status',
                    allowEditing: false,
                    lookup: {
                        dataSource: statusList,
                        displayExpr: "name",
                        valueExpr: "id"
                    }
                },
                {
                    caption: 'Tgl_SP2D',
                    dataType: 'date',
                    allowFiltering: false,
                    dataField: 'Tgl_SP2D',
                    allowEditing: false,
                }, {
                    caption: 'No_SP2D',
                    allowFiltering: true,
                    dataField: 'No_SP2D',
                    allowEditing: false,
                }, {
                    caption: 'No_Rekening',
                    allowFiltering: false,
                    dataField: 'No_Rekening',
                    allowEditing: false,
                }, {
                    caption: 'Nm_Penerima',
                    allowFiltering: false,
                    dataField: 'Nm_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'NPWP',
                    allowFiltering: false,
                    dataField: 'NPWP',
                    allowEditing: false,
                }, {
                    caption: 'Rek_Penerima',
                    allowFiltering: false,
                    dataField: 'Rek_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'Bank_Penerima',
                    allowFiltering: false,
                    dataField: 'Bank_Penerima',
                    allowEditing: false,
                }, {
                    caption: 'id_teller',
                    allowFiltering: false,
                    dataField: 'id_teller',
                    allowEditing: false,
                }, {
                    caption: 'id_supervisor',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'id_supervisor',
                    allowEditing: false,
                }, {
                    caption: 'tglSupervisi',
                    allowFiltering: false,
                    dataField: 'tglSupervisi',
                    allowEditing: false,
                }, {
                    caption: 'No_SPM',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'No_SPM',
                    allowEditing: false,
                }, {
                    caption: 'Tgl_SPM',
                    visible: false,
                    dataType: 'date',
                    allowFiltering: false,
                    dataField: 'Tgl_SPM',
                    allowEditing: false,
                }, {
                    caption: 'Jn_SPM',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Jn_SPM',
                    allowEditing: false,
                }, {
                    caption: 'Keterangan',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Keterangan',
                    allowEditing: false,
                }, {
                    caption: 'Tgl_Penguji',
                    visible: false,
                    allowFiltering: false,
                    dataType: 'date',
                    dataField: 'Tgl_Penguji',
                    allowEditing: false,
                }, {
                    caption: 'Nm_Bank',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Nm_Bank',
                    allowEditing: false,
                }, {
                    caption: 'Nilai',
                    allowFiltering: false,
                    dataField: 'Nilai',
                    allowEditing: false,
                    alignment: "right",
                    calculateCellValue: function (e) {
                        angka = e.Nilai;
                        var rupiah = '';
                        var angkarev = angka.toString().split('').reverse().join('');
                        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                        return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');

                    }
                }, {
                    caption: 'DateCreate',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'DateCreate',
                    allowEditing: false,
                }, {
                    caption: 'Cair',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Cair',
                    allowEditing: false,
                }, {
                    caption: 'TglCair',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'TglCair',
                    allowEditing: false,
                }, {
                    caption: 'Gaji',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Gaji',
                    allowEditing: false,
                }, {
                    caption: 'Nm_Unit',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Nm_Unit',
                    allowEditing: false,
                }, {
                    caption: 'Nm_Sub_Unit',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Nm_Sub_Unit',
                    allowEditing: false,
                }, {
                    caption: 'Uraian',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Uraian',
                    allowEditing: false,
                }, {
                    caption: 'Tahun',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Tahun',
                    allowEditing: false,
                }, {
                    caption: 'Kd_Urusan',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Kd_Urusan',
                    allowEditing: false,
                }, {
                    caption: 'Kd_Bidang',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Kd_Bidang',
                    allowEditing: false,
                }, {
                    caption: 'Kd_Unit',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Kd_Unit',
                    allowEditing: false,
                }, {
                    caption: 'Kd_Sub',
                    visible: false,
                    allowFiltering: false,
                    dataField: 'Kd_Sub',
                    allowEditing: false,
                },
            ],
            onSelectionChanged: function (selectedItems) {
                if (selectedItems.selectedRowsData[0].No_SP2D) {
                    selectedId = selectedItems.selectedRowsData[0].No_SP2D;
                    status = selectedItems.selectedRowsData[0].status;
                }
            },
            onRowClick: function (e) {
                /*lastRowCLickedId = e.rowIndex;
                 console.log(e);*/
            },
            onRowPrepared: function (info) {
                if (info.rowType === 'data') {
                    if (info.data.status == 1) {
                        info.rowElement.css('background', '#FFF');
                    } else if (info.data.Status == 2) {
                        info.rowElement.css('background', '#E8F5E9');//
                    }
                    else if (info.data.Status == 3) {
                        info.rowElement.css('background', '#FFEBEE');
                    }
                    else if (info.data.Status == 4) {
                        info.rowElement.css('background', '#E8F5E9');
                    }

                }
            }

        }).dxDataGrid("instance");

        $("#btn_proses_sp2d").click(function () {
            console.log(selectedId);
            if (selectedId) {
                $.ajax({
                    url: "pencairan-tindakan",
                    method: 'POST',
                    data: {
                        id: selectedId,
                        tindakan: "setuju",
                        id_supervisor: id_supervisor
                    },
                    success: function (result) {
                        DevExpress.ui.notify('SP2D berhasil disetujui', 'success', 600);
                        grid.refresh();
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                // $(location).attr('href', 'sp2d-detail?id=' + selectedId);
            } else {
                DevExpress.ui.notify('Silahkan pilih data dulu', 'warning', 600);
            }
        });


        $("#btn_cairkan_sp2d").click(function () {

            if (selectedId) {
                console.log(selectedId);
                console.log(status);
                if (status && status == 1) {
                    DevExpress.ui.notify('Masih menunggu konfirmasi', 'warning', 1000);
                } else if (status && status == 2) {
                    $.ajax({
                        url: "pencairan-tindakan",
                        method: 'POST',
                        data: {
                            id: selectedId,
                            id_supervisor: id_supervisor,
                            tindakan: "cair"
                        },
                        success: function (result) {
                            $(location).attr('href', 'sp2d-invoice?id=' + selectedId);
                        },
                        error: function () {
                            DevExpress.ui.notify('Error,Tidak dapat mencairkan', 'warning', 1000);
                        },
                        // timeout: 7000
                    });

                } else if (status && status == 4) {
                    DevExpress.ui.notify('SP2D yang sudah dicairkan', 'warning', 1000);
                }else {
                    DevExpress.ui.notify('SP2D yang dipilih tidak dapat dicairkan', 'warning', 1000);
                }
            } else {
                DevExpress.ui.notify('Silahkan pilih data dulu', 'warning', 1000);
            }
        });

        $("#btn_tolak_sp2d").click(function () {
            console.log(selectedId);
            if (selectedId) {
                $.ajax({
                    url: "pencairan-tindakan",
                    method: 'POST',
                    data: {
                        id: selectedId,
                        id_supervisor: id_supervisor,
                        tindakan: "tolak"
                    },
                    success: function (result) {
                        DevExpress.ui.notify('SP2D berhasil ditolak', 'success', 600);
                        grid.refresh();

                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    // timeout: 7000
                });
                //$(location).attr('href', 'sp2d-detail?id=' + selectedId);
            } else {
                DevExpress.ui.notify('Silahkan pilih data dulu', 'warning', 600);
            }
        });

        window.setInterval(function(){
            grid.refresh();
        },10000);

    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>