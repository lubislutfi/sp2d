<?php $__env->startSection('title', 'Pencairan'); ?>
<?php $__env->startSection('content'); ?>
<div id="card_input_id" class="row">
    <div class="col-md-6 offset-lg-3">
        <div class="card">
            <div class="card-header card-primary text-white">
                Input Nomor ID
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-25">
                            <div class="col-lg-10 push-lg-1">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-id-card"></i></span>
                                    <input type="text" class="form-control" id="id_sp2d" placeholder="Nomor ID">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-11 text-right">
                                <a id="btn_input_id_clear" class="btn btn-warning text-white">Clear</a>
                                <a id="btn_input_id_cek" class="btn btn-primary text-white">CEK</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div id="pb_input_id" class="progress" style="display: none">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 100%"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Mengambil data
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="card_inquiry_data" class="row" style="display: none">
    <div class="col-md-6 offset-lg-3">
        <div class="card">
            <div class="card-header card-primary text-white">
                Nomor ID ditemukan
            </div>
            <div class="card-block">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="username2" class="col-form-label form_lay_email2">Username</label>
                            </div>
                            <div class="col-lg-8 user_icon_change2">
                                <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                    <input type="text" class="form-control form_lay_input2" id="username2"
                                           placeholder="Username">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="username2" class="col-form-label form_lay_email2">Username</label>
                            </div>
                            <div class="col-lg-8 user_icon_change2">
                                <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                    <input type="text" class="form-control form_lay_input2" id="username2"
                                           placeholder="Username">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="username2" class="col-form-label form_lay_email2">Username</label>
                            </div>
                            <div class="col-lg-8 user_icon_change2">
                                <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                    <input type="text" class="form-control form_lay_input2" id="username2"
                                           placeholder="Username">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="username2" class="col-form-label form_lay_email2">Username</label>
                            </div>
                            <div class="col-lg-8 user_icon_change2">
                                <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                    <input type="text" class="form-control form_lay_input2" id="username2"
                                           placeholder="Username">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="username2" class="col-form-label form_lay_email2">Username</label>
                            </div>
                            <div class="col-lg-8 user_icon_change2">
                                <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                    <input type="text" class="form-control form_lay_input2" id="username2"
                                           placeholder="Username">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-11 text-right">
                                <a id="btn_cairkan" class="btn btn-primary text-white">CAIRKAN</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="card_invoice" class="card" style="display: none">
    <div class="card-header  card-primary text-white">
        <i class="icon ion-card qwe" data-pack="default"
           data-tags="credit, price, debit, money, shopping, cash, dollars, $"></i>
        Invoice for Purchase #33221
    </div>
    <div class="card-block m-t-35">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 mrg_btm15">
                        This Invoice is Generated For puchase of Items listed below and the order id is #33221. And the
                        payment detaisl as follows, Billing details and shipping details are mentioned below.
                        <br>
                        for any enquiries about this Invoice document feel free to ask your queries by the given mobile
                        detials or post your queries by the given mail id.
                        <br>
                        And the delivery details and delivery status are
                        <br>
                        <br>
                        Order Date :
                        <strong>25th AUG 2016</strong>
                        <br>
                        Order Status :
                        <strong>Shipped</strong>
                        <br>
                        Order delivery :
                        <strong>25th AUg 2016</strong>
                        expecting
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 invoice_body_billing_details">
                <div class="row justify-content-lg-between">
                    <div class="col-lg-4">
                        <div class="invoice_details">
                            <h4 class="success_txt">Billing Details:</h4>
                            <strong>Ms Natalya Pery</strong>
                            <br>
                            Main Address &amp; Streen Num
                            <br>
                            Streen Name &amp; City Details
                            <br>
                            State Details
                            <br>
                            Phone Num: XXX-XXX-XXXX
                            <br>
                            Mail Id: mailid@mail.com
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="invoice_details">
                            <span class="info_txt">Shipping Details:</span><br>
                            <strong>Ms Natalya Pery</strong>
                            <br>
                            Main Address &amp; Streen Num
                            <br>
                            Streen Name &amp; City Details
                            <br>
                            State Details
                            <br>
                            Phone Num: XXX-XXX-XXXX
                            <br>
                            Mail Id: mailid@mail.com
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>Order Summary</strong>
                    </div>
                    <div class="card-block m-t-35">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <td>
                                        <strong>Item Name</strong>
                                    </td>
                                    <td>
                                        <strong>Price</strong>
                                    </td>
                                    <td class="text-center">
                                        <strong>Item Quantity</strong>
                                    </td>
                                    <td></td>
                                    <td class="text-right">
                                        <strong>Total</strong>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Samsung Galaxy Grand</td>
                                    <td>$700</td>
                                    <td class="text-center">1</td>
                                    <td></td>
                                    <td class="text-right">$700</td>
                                </tr>
                                <tr>
                                    <td>Samsung Galaxy Core 2</td>
                                    <td>$1110</td>
                                    <td class="text-center">1</td>
                                    <td></td>
                                    <td class="text-right">$1110</td>
                                </tr>
                                <tr>
                                    <td>Screen Protector</td>
                                    <td>$7</td>
                                    <td class="text-center">4</td>
                                    <td></td>
                                    <td class="text-right">$28</td>
                                </tr>
                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"></td>
                                    <td class="highrow text-right">
                                        <strong>Sub Total &nbsp;</strong>
                                    </td>
                                    <td class="highrow text-right">
                                        <strong>$1838</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"></td>
                                    <td class="emptyrow text-right">
                                        <strong>Shipping &nbsp;</strong>
                                    </td>
                                    <td class="highrow text-right">
                                        <strong>$20</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="emptyrow">
                                        <i class="livicon" data-name="barcode" data-size="60" data-loop="true"></i>
                                    </td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"></td>
                                    <td class="emptyrow text-right">
                                        <strong>Total &nbsp;</strong>
                                    </td>
                                    <td class="highrow text-right">
                                        <strong>$1858</strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 invoice_print">
                <span class="pull-right">
                    <a id="btn_invoice_selesai" class="btn button-alignment btn-warning text-white m-t-15">
                        Selesai
                    </a>
                    <a style="color:#fff;" class="btn button-alignment btn-info m-t-15" data-toggle="button"
                       onclick="javascript:window.print();">
                        Cetak
                    </a>

                </span>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        PNotify.prototype.options.delay = 1500;

        $("#btn_input_id_clear").click(function () {
            $('#id_sp2d').val('');
        });

        $("#btn_input_id_cek").click(function () {

            if ($('#id_sp2d').val() == "") {

                new PNotify({
                    title: 'Periksa Kembali',
                    text: 'Nomor ID kosong, Pastikan sudah terisi',
                    type: 'info',
                    styling: 'brighttheme',
                    animate: {
                        animate: true,
                        in_class: 'bounceIn',
                        out_class: 'bounceOut'
                    }
                });
            } else {
                console.log($('#id_sp2d').val());
                $('#card_input_id').css('display', 'none');
                $('#card_inquiry_data').css('display', 'block');
                $('#card_invoice').css('display', 'none');
            }
        });

        $("#btn_cairkan").click(function () {
            $('#id_sp2d').val('');
            $('#card_input_id').css('display', 'none');
            $('#card_inquiry_data').css('display', 'none');
            $('#card_invoice').css('display', 'block');
        });

        $("#btn_invoice_selesai").click(function () {
            $('#id_sp2d').val('');
            $('#card_input_id').css('display', 'block');
            $('#card_inquiry_data').css('display', 'none');
            $('#card_invoice').css('display', 'none');
        });
    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>