<li class="<?php echo e(Request::is('/') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/')); ?>">
        <i class="fa fa-home"></i>
        <span class="link-title menu_hide">&nbsp;Dashboard</span>
    </a>
</li>
<li class="<?php echo e(Request::is('pencairan') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('pencairan')); ?>">
        <i class="fa fa-pencil"></i>
        <span class="link-title menu_hide">&nbsp;Pencairan</span>
    </a>
</li>
<li class="dropdown_menu <?php echo e(Request::is('log-sukses')||Request::is('log-semua') ? 'active' : ''); ?>">
    <a href="javascript:;">
        <i class="fa fa-server"></i>
        <span class="link-title menu_hide">&nbsp; History</span>
        <span class="fa arrow menu_hide"></span>
    </a>
    <ul>
        <li class="<?php echo e(Request::is('log-sukses') ? 'active' : ''); ?>">
            <a href="<?php echo e(url('/log-sukses')); ?>">
                <i class="fa fa-angle-right"></i>
                &nbsp; Pencairan Sukses
            </a>
        </li>
        <li class="<?php echo e(Request::is('log-semua') ? 'active' : ''); ?>">
            <a href="log-semua">
                <i class="fa fa-angle-right"></i>
                <span class="link-title">&nbsp;Log</span>
            </a>
        </li>
    </ul>
</li>
<li class="<?php echo e(Request::is('users') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/users')); ?>">
        <i class="fa fa-key"></i>
        <span class="link-title menu_hide">&nbsp;Users</span>
    </a>
</li>