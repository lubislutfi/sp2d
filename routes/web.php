<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

/*
Route::get('masuk', 'UsersController@loginIndex');
Route::post('masuk', 'UsersController@loginAuthenticate');*/

Route::get('login', ['as' => 'masuk', 'uses' => 'UsersController@loginIndex']);
Route::post('login', ['as' => 'masuk', 'uses' => 'UsersController@loginAuthenticate']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('logout', ['as' => 'logout', 'uses' => 'UsersController@logoutAuthenticate']);

    Route::resource('users', 'UsersController'); //user resource
    Route::resource('branches-list', 'BranchesController'); //user resource

    Route::get('/pencairan', 'PencairanController@index');
    Route::get('/log-semua', 'LogController@index_log_semua');
    Route::get('/log-sukses', 'LogController@index_log_sukses');
    Route::get('/users', 'UsersController@index');

    Route::get('/sp2d-list', 'TRXSP2DController@index');
    Route::get('/spm-list', 'TRXSPMController@index');
    Route::get('/sp2d-detail', 'TRXSP2DController@indexDetailSP2D');
    Route::get('/sp2d-invoice', 'TRXSP2DController@indexInvoiceSP2D');
    Route::resource('/pencairan-list', 'PencairanController');

    Route::post('/pencairan-tindakan', 'PencairanController@pencairanTindakan'); //get sp2d

    //api
    Route::post('/gettrxsp2d', 'TRXSP2DController@getTrxSP2D'); //get sp2d
    Route::post('/getdetailtrxsp2d', 'TRXSP2DController@getDetailTrxSP2D'); //get detail sp2d
    Route::post('/withdrawtrxsp2d', 'TRXSP2DController@withdrawTrxSP2D'); //pencairan sp2d
    Route::post('/getjsonbranches', 'BranchesController@getJsonBranches'); //get cabang
    Route::post('/getjsonpencairan', 'PencairanController@getJsonPencairan'); //get cabang

    Route::post('/gettrxspm', 'TRXSPMController@getTrxSPM'); //get spm
    Route::get('/getjsonuser', 'UsersController@getJsonUser'); //get user
});

/*Route::group(['middleware' => ['sp2dMiddleware']], function () {

});*/


/*Route::get('/phpinfo', function () {
    phpinfo();
});

Route::post('/tesdb', function () {
    //return DB::connection('sqlsrv')->table('TrxSP2D')->select('Nm_Unit')->get();
    return "tes respon";
});*/

//Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

// Authentication Routes...

//Route::get('/home', 'HomeController@index')->name('home');
